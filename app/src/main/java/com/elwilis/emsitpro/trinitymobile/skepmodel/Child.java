package com.elwilis.emsitpro.trinitymobile.skepmodel;

public class Child {

    private String Name, Caption;

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getCaption() {
        return Caption;
    }

    public void setCaption(String Caption) {
        this.Caption = Caption;
    }
}
