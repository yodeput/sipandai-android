package com.elwilis.emsitpro.trinitymobile;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elwilis.emsitpro.trinitymobile.livereport.NoteActivity;
import com.elwilis.emsitpro.trinitymobile.livereport.PesanHolder;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * Created by rezar on 16/02/18.
 */

public class PesanAdapter extends RecyclerView.Adapter<PesanHolder>  {

    Context context;
    Intent intent;
    List<PesanObject> msgData;

    public PesanAdapter(List<PesanObject> pesanObjectsList, Context context){
        super();
        this.msgData = pesanObjectsList;
        this.context = context;
    }

    @Override
    public PesanHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_pesan_item, parent, false);
        PesanHolder holder = new PesanHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(PesanHolder holder, final int position) {
        PesanObject pesanObject = msgData.get(position);

        holder.subj_laps.setText(pesanObject.getSubjLaps());
        holder.kant_kpbc.setText(pesanObject.getNamaKant());
        holder.nama_comp.setText(pesanObject.getNamaComp());
        holder.nama_user.setText(pesanObject.getNamaUser());
        holder.time_laps.setText(pesanObject.getDateNote());
        holder.seqs_note.setText(pesanObject.getSeqsNote());
        holder.refs_sess.setText(pesanObject.getRefsSess());

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View rootView, int i) {
                Intent myIntent = new Intent(context, NoteActivity.class);
                myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                myIntent.putExtra("seqs_note", msgData.get(i).getSeqsNote());
                myIntent.putExtra("refs_sess", msgData.get(i).getRefsSess());
                context.startActivity(myIntent);
            }
        });

        if (pesanObject.getRefsRead().contains("true")) {
            holder.subj_laps.setTextColor(Color.parseColor("#c0c7de"));
        }
    }

    @Override
    public int getItemCount() {
        return msgData.size();
    }

}
