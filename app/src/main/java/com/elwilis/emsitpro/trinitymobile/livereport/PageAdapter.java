package com.elwilis.emsitpro.trinitymobile.livereport;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.elwilis.emsitpro.trinitymobile.R;

import java.util.List;

public class PageAdapter extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<SlideUtils> slideImg;
    private ImageLoader imageLoader;

    public PageAdapter(List slideImg, Context context) {
        this.slideImg = slideImg;
        this.context = context;
    }

    @Override
    public int getCount() {
        return slideImg.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.imgview_fas, null);

        SlideUtils utils = slideImg.get(position);

        ImageView imageView = (ImageView) view.findViewById(R.id.img_view);

        imageLoader = VolleyRequest.getInstance(context).getImageLoader();
        imageLoader.get(utils.getSlideImageUrl(), ImageLoader.getImageListener(imageView, R.mipmap.img_empty, R.mipmap.img_empty));


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if(position == 0){
//                    Toast.makeText(context, "Slide 1 Clicked", Toast.LENGTH_SHORT).show();
//                } else if(position == 1){
//                    Toast.makeText(context, "Slide 2 Clicked", Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(context, "Slide 3 Clicked", Toast.LENGTH_SHORT).show();
//                }

            }
        });

        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }

}
