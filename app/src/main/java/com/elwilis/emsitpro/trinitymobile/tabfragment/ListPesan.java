package com.elwilis.emsitpro.trinitymobile.tabfragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.elwilis.emsitpro.trinitymobile.AppVar;
import com.elwilis.emsitpro.trinitymobile.PesanAdapter;
import com.elwilis.emsitpro.trinitymobile.PesanObject;
import com.elwilis.emsitpro.trinitymobile.R;
import com.elwilis.emsitpro.trinitymobile.SessionManage;
import com.elwilis.emsitpro.trinitymobile.livereport.QuickReportActivity;
import com.rilixtech.materialfancybutton.MaterialFancyButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


/**
 * Created by rezar on 06/02/18.
 */

public class ListPesan extends Fragment implements RecyclerView.OnScrollChangeListener {

    // general
    Context context;

    // list pesan
    List<PesanObject> pesanObjectsList;

    // view
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;

    RequestQueue requestQueue;
    ProgressDialog pDialog;
    SessionManage session;

    // holder
    int requestPages = 1;
    String apiEnd, tokEns, usrNip;
    Map<String, String> headers;


    public static ListPesan newInstance() {
        return new ListPesan();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tab_pesan, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        session = new SessionManage(Objects.requireNonNull(getActivity()).getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();

        tokEns = user.get(SessionManage.KEY_NAME);
        usrNip = user.get(SessionManage.KEY_NIP);
        pDialog = new ProgressDialog(getActivity());
        context = getActivity().getApplicationContext();

        MaterialFancyButton fancyButton = (MaterialFancyButton) getActivity().findViewById(R.id.reloRepo);
        fancyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pesanObjectsList = new ArrayList<>();
                requestQueue = Volley.newRequestQueue(context);
                requestPages = 1;
                getData();
                adapter = new PesanAdapter(pesanObjectsList, context);
                recyclerView.setAdapter(adapter);
            }
        });

        FloatingActionButton fab = getActivity().findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intenReport = new Intent(context, QuickReportActivity.class);
                intenReport.putExtra("userNips", usrNip);
                startActivity(intenReport);
            }
        });

        pDialog.setMessage("Sedang memuat...");
        showDialog();

        recyclerView = (RecyclerView) getActivity().findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);

        // init list
        pesanObjectsList = new ArrayList<>();
        requestQueue = Volley.newRequestQueue(context);

        getData();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            recyclerView.setOnScrollChangeListener(this);
        }
        adapter = new PesanAdapter(pesanObjectsList, context);
        recyclerView.setAdapter(adapter);
    }

    private JsonObjectRequest loadJSON(final int requestPages) {
        showDialog();
        apiEnd = AppVar.LIVE_RP;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                apiEnd,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideDialog();
                        try {
                            Boolean sukses = response.getBoolean("success");
                            String pesan   = response.getString("message");
                            if (sukses) {
                                JSONObject data = response.getJSONObject("data");
                                JSONArray hasil = data.getJSONArray("result");
                                parsePesan(hasil);
                                hideDialog();
                            } else {
                                Toast.makeText(context, pesan, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError){
                    Toast.makeText(context, "Connection time out", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(context, "Server problem", Toast.LENGTH_SHORT).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(context, "Internet problem", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(context, "Parsing problem", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Semua pesan sudah ditampilkan.", Toast.LENGTH_SHORT).show();
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() {
                headers = new HashMap<String, String>();
                headers.put("x-access-token", tokEns);
                headers.put("pages", String.valueOf(requestPages));
                headers.put("users", usrNip);
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        return jsonObjectRequest;
    }

    private void getData() {
        requestQueue.add(loadJSON(requestPages));
        requestPages++;
    }

    private void parsePesan(JSONArray hasil) {
        for (int i = 0; i < hasil.length(); i++) {
            PesanObject pesanObject = new PesanObject();
            JSONObject jsonObject   = null;
            try {
                jsonObject = hasil.getJSONObject(i);
                pesanObject.setSubjLaps(jsonObject.getString("jud_note"));
                pesanObject.setNamaKant(cekNull(jsonObject.getString("ref_kant")) + " / " + jsonObject.getString("ref_fasi"));
                pesanObject.setNamaComp(jsonObject.getString("ref_comp"));
                pesanObject.setNamaUser(jsonObject.getString("nam_user") + " - " + tglHelper(jsonObject.getString("cre_date")));
                pesanObject.setDateNote(tglHelper(jsonObject.getString("cre_date")));
                pesanObject.setSeqsNote(String.valueOf(jsonObject.getInt("seq_note")));
                pesanObject.setRefsSess(jsonObject.getString("ref_sess"));
                pesanObject.setRefsRead(String.valueOf(jsonObject.getString("ref_stat")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            pesanObjectsList.add(pesanObject);
        }
        adapter.notifyDataSetChanged();
    }

    private boolean isLastItemDisplaying(RecyclerView recyclerView) {
        if (recyclerView.getAdapter().getItemCount() != 0) {
            int lastVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
            if (lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == recyclerView.getAdapter().getItemCount() - 1)
                return true;
        }
        return false;
    }

    @Override
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        if (isLastItemDisplaying(recyclerView)) {
            getData();
        }
    }

    // show dialog method
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    // hide dialog method
    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private String tglHelper(String params){
        String[] sourceParams = params.split("-");
        String tahun = sourceParams[0];
        String bulan = sourceParams[1];
        String tangg = sourceParams[2].substring(0,2);

        String[] WaktuParams = params.split(" ");
        String waktu = WaktuParams[0].substring(11);
        String[] detWak = waktu.split(":");
        String jam = detWak[0];
        String menit = detWak[1];
        String detik = detWak[2].substring(0,2);

        return tangg + "/" + bulan + "/" + tahun + " " + jam + ":" + menit + ":" + detik + " WIB";
    }

    private String cekNull (String params) {
        if (params != null && !params.isEmpty() && params != "null") {
            return params;
        } else {
            return "-";
        }
    }

}
