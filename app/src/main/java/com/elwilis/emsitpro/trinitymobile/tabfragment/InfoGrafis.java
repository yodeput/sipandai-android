package com.elwilis.emsitpro.trinitymobile.tabfragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.elwilis.emsitpro.trinitymobile.R;
import com.elwilis.emsitpro.trinitymobile.SessionManage;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rezar on 06/02/18.
 */

public class InfoGrafis extends Fragment {

    Spinner optKantor;
    SessionManage session;
    String grpHol, kpbHol;

    // bindview
    @BindView(R.id.textKpbc) TextView _textkpbc;

    public static InfoGrafis newInstance() {
        return new InfoGrafis();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.tab_infografis, container, false);
        final View kpbcView = inflater.inflate(R.layout.tab_infografis_kpbc, container, false);

        ButterKnife.bind(this, kpbcView);

        session = new SessionManage(Objects.requireNonNull(getActivity()).getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        grpHol = user.get(SessionManage.KEY_GRUP);
        kpbHol = user.get(SessionManage.KEY_KPBC);

        optKantor = rootView.findViewById(R.id.optKanDash);
        ArrayAdapter<CharSequence> adapterKan = ArrayAdapter.createFromResource(rootView.getContext(),
                R.array.optKantor, android.R.layout.simple_spinner_item);
        adapterKan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        optKantor.setAdapter(adapterKan);

        optKantor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id)
            {
                // String selected = parentView.getItemAtPosition(position).toString();
                // Context context = parentView.getContext();
                // CharSequence text = selected;
                // int duration = Toast.LENGTH_SHORT;
                // Toast toast = Toast.makeText(context, text, duration);
                // toast.show();
                switch (position){
                    case 0:
                        loadFragKwbc();
                        break;
                    case 1:
                        loadFragBcckr();
                        break;
                    case 2:
                        loadFragBcbdg();
                        break;
                    case 3:
                        loadFragBcbks();
                        break;
                    case 4:
                        loadFragBcbgr();
                        break;
                    case 5:
                        loadFragBcpwk();
                        break;
                    case 6:
                        loadFragBccrb();
                        break;
                    case 7:
                        loadFragBctsk();
                        break;
                    default:
                        break;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        if (kpbHol.equals("050000"))
        {
            return rootView;
        } else {
            if (kpbHol.equals("050300"))
            {
                loadFragBcbgr();
                _textkpbc.setText("KPPBC TMP A Bogor");
            } else if (kpbHol.equals("050500")) {
                loadFragBcbdg();
                _textkpbc.setText("KPPBC TMP A Bandung");
            } else if (kpbHol.equals("050600")) {
                loadFragBctsk();
                _textkpbc.setText("KPPBC TMP C Tasikmalaya");
            } else if (kpbHol.equals("050700")) {
                loadFragBccrb();
                _textkpbc.setText("KPPBC TMP C Cirebon");
            } else if (kpbHol.equals("050800")) {
                loadFragBcpwk();
                _textkpbc.setText("KPPBC TMP A Purwakarta");
            } else if (kpbHol.equals("050900")) {
                loadFragBcbks();
                _textkpbc.setText("KPPBC TMP A Bekasi");
            } else {
                loadFragBcckr();
                _textkpbc.setText("KPPBC TMP Cikarang");
            }
            return kpbcView;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
    }

    private void loadFragKwbc(){
        FragKwbcDash fragment = FragKwbcDash.newInstance();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_infografis, fragment);
        ft.commit();
    }

    private void loadFragBcbgr(){
        FragBcbgrDash fragment = FragBcbgrDash.newInstance();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_infografis, fragment);
        ft.commit();
    }

    private void loadFragBcbdg(){
        FragBcbdgDash fragment = FragBcbdgDash.newInstance();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_infografis, fragment);
        ft.commit();
    }

    private void loadFragBctsk(){
        FragBctskDash fragment = FragBctskDash.newInstance();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_infografis, fragment);
        ft.commit();
    }

    private void loadFragBccrb(){
        FragBccrbDash fragment = FragBccrbDash.newInstance();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_infografis, fragment);
        ft.commit();
    }

    private void loadFragBcpwk(){
        FragBcpwkDash fragment = FragBcpwkDash.newInstance();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_infografis, fragment);
        ft.commit();
    }

    private void loadFragBcbks(){
        FragBcbksDash fragment = FragBcbksDash.newInstance();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_infografis, fragment);
        ft.commit();
    }

    private void loadFragBcckr(){
        FragBcckrDash fragment = FragBcckrDash.newInstance();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_infografis, fragment);
        ft.commit();
    }
}
