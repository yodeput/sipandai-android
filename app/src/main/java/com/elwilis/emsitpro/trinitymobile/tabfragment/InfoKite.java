package com.elwilis.emsitpro.trinitymobile.tabfragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.numberprogressbar.NumberProgressBar;
import com.elwilis.emsitpro.trinitymobile.AppVar;
import com.elwilis.emsitpro.trinitymobile.R;
import com.elwilis.emsitpro.trinitymobile.SessionManage;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InfoKite extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar _toolbar;
    @BindView(R.id.kbTitle) TextView _kbtitle;
    @BindView(R.id.kbLast) TextView _kblast;
    @BindView(R.id.item_counter) TextView _sumpt;
    @BindView(R.id.item_counter2) TextView _sumpkb;
    @BindView(R.id.item_counter3) TextView _sumukb;
    @BindView(R.id.infoKbAktif) TextView _sumakt;
    @BindView(R.id.infoKbBeku) TextView _sumbku;
    @BindView(R.id.infoKbCabut) TextView _sumcbt;
    @BindView(R.id.txtKat1) TextView _kat1;
    @BindView(R.id.item_cnt1) TextView _tkat1;
    @BindView(R.id.txtKat2) TextView _kat2;
    @BindView(R.id.item_cnt2) TextView _tkat2;
    @BindView(R.id.txtKat3) TextView _kat3;
    @BindView(R.id.item_cnt3) TextView _tkat3;
    @BindView(R.id.txtSka1) TextView _sub1;
    @BindView(R.id.item_cnt4) TextView _tsub1;
    @BindView(R.id.txtSka2) TextView _sub2;
    @BindView(R.id.item_cnt5) TextView _tsub2;
    @BindView(R.id.txtSka3) TextView _sub3;
    @BindView(R.id.item_cnt6) TextView _tsub3;
    @BindView(R.id.progBar1) NumberProgressBar _prog1;
    @BindView(R.id.progBar2) NumberProgressBar _prog2;
    @BindView(R.id.progBar3) NumberProgressBar _prog3;
    @BindView(R.id.progBar4) NumberProgressBar _prog4;
    @BindView(R.id.progBar5) NumberProgressBar _prog5;
    @BindView(R.id.progBar6) NumberProgressBar _prog6;
    @BindView(R.id.pieRsk) PieChart _grisk;
    @BindView(R.id.pieLok) PieChart _gloka;
    @BindView(R.id.pieItv) PieChart _gitiv;
    @BindView(R.id.barCnt) BarChart _gcntr;
    @BindView(R.id.barFou) BarChart _gfoul;

    // general
    Context context;
    SessionManage session;
    ProgressDialog pDialog;
    NumberFormat numFor;
    Locale locale;
    Intent intent;
    Bundle bundle;

    // holder & adapter
    String apiEnd, token, formatDateTime, extHol;
    Map<String, String> headers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_kite);
        ButterKnife.bind(this);
        setSupportActionBar(_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        context = InfoKite.this;
        locale  = new Locale("en","EN");
        session = new SessionManage(context);

        HashMap<String, String> user = session.getUserDetails();
        token   = user.get(SessionManage.KEY_NAME);

        numFor  = NumberFormat.getNumberInstance(locale);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Mohon tunggu...");
        showDialog();

        if (savedInstanceState == null) {
            bundle = getIntent().getExtras();
            if (bundle == null) {
                extHol = "";
            } else {
                extHol = bundle.getString("refkan");
            }
        }

        // graph risiko
        _grisk.setUsePercentValues(true);
        _grisk.getDescription().setEnabled(false);
        _grisk.setDrawHoleEnabled(true);
        _grisk.setHoleColor(Color.parseColor("#2E3858"));
        _grisk.setTransparentCircleColor(Color.WHITE);
        _grisk.setTransparentCircleAlpha(110);
        _grisk.setHoleRadius(48f);
        _grisk.setTransparentCircleRadius(50f);
        _grisk.setDrawCenterText(true);
        _grisk.setRotationAngle(0);
        _grisk.setRotationEnabled(true);
        _grisk.setHighlightPerTapEnabled(true);
        _grisk.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        _grisk.setDrawEntryLabels(false);

        Legend riskLeg = _grisk.getLegend();
        riskLeg.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        riskLeg.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        riskLeg.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        riskLeg.setDrawInside(false);
        riskLeg.setXEntrySpace(10f);
        riskLeg.setYEntrySpace(0f);
        riskLeg.setYOffset(5f);
        riskLeg.setTextColor(Color.WHITE);

        // lokasi
        _gloka.setUsePercentValues(true);
        _gloka.getDescription().setEnabled(false);
        _gloka.setDrawHoleEnabled(true);
        _gloka.setHoleColor(Color.parseColor("#2E3858"));
        _gloka.setTransparentCircleColor(Color.WHITE);
        _gloka.setTransparentCircleAlpha(110);
        _gloka.setHoleRadius(48f);
        _gloka.setTransparentCircleRadius(50f);
        _gloka.setDrawCenterText(true);
        _gloka.setRotationAngle(0);
        _gloka.setRotationEnabled(true);
        _gloka.setHighlightPerTapEnabled(true);
        _gloka.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        _gloka.setDrawEntryLabels(false);

        Legend lokaLeg = _gloka.getLegend();
        lokaLeg.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        lokaLeg.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        lokaLeg.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        lokaLeg.setDrawInside(false);
        lokaLeg.setXEntrySpace(10f);
        lokaLeg.setYEntrySpace(0f);
        lokaLeg.setYOffset(5f);
        lokaLeg.setTextColor(Color.WHITE);

        // it inventory
        _gitiv.setUsePercentValues(true);
        _gitiv.getDescription().setEnabled(false);
        _gitiv.setDrawHoleEnabled(true);
        _gitiv.setHoleColor(Color.parseColor("#2E3858"));
        _gitiv.setTransparentCircleColor(Color.WHITE);
        _gitiv.setTransparentCircleAlpha(110);
        _gitiv.setHoleRadius(48f);
        _gitiv.setTransparentCircleRadius(50f);
        _gitiv.setDrawCenterText(true);
        _gitiv.setRotationAngle(0);
        _gitiv.setRotationEnabled(true);
        _gitiv.setHighlightPerTapEnabled(true);
        _gitiv.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        _gitiv.setDrawEntryLabels(false);

        Legend itivLeg = _gitiv.getLegend();
        itivLeg.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        itivLeg.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        itivLeg.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        itivLeg.setDrawInside(false);
        itivLeg.setXEntrySpace(10f);
        itivLeg.setYEntrySpace(0f);
        itivLeg.setYOffset(5f);
        itivLeg.setTextColor(Color.WHITE);

        // negara pic
        Legend cntrLeg = _gcntr.getLegend();
        cntrLeg.setEnabled(false);

        // foul
        Legend foulLeg = _gfoul.getLegend();
        foulLeg.setEnabled(false);

        jsonData(extHol);

    }

    private void jsonData(final String extHol) {
        if (!"050000".equals(extHol)) {
            apiEnd = AppVar.BCKT_URL;
        } else {
            apiEnd = AppVar.KWKT_URL;
        }
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                apiEnd,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideDialog();
                        try {
                            Boolean sukses = response.getBoolean("success");
                            String pesan = response.getString("message");
                            if (sukses) {
                                JSONObject data = response.getJSONObject("data");

                                // header
                                JSONArray lastu = data.getJSONArray("_lastu");
                                JSONObject _data = lastu.getJSONObject(0);
                                _kbtitle.setText(_data.getString("kant"));
                                final String vDate = _data.getString("last");

                                Instant instant = null;
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                                    instant = Instant.parse(vDate);
                                    LocalDateTime result = LocalDateTime.ofInstant(instant, ZoneId.of(ZoneOffset.UTC.getId()));
                                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
                                    formatDateTime = "Diperbarui tanggal: " + result.format(formatter);
                                } else {
                                    formatDateTime = "Diperbarui tanggal: " + tglHelper(vDate);
                                }
                                _kblast.setText(formatDateTime);

                                // sum
                                JSONArray jumla = data.getJSONArray("_jumla");
                                JSONObject _jum = jumla.getJSONObject(0);
                                _sumpt.setText(_jum.getString("total"));
                                _sumpkb.setText(_jum.getString("bbs"));
                                _sumukb.setText(_jum.getString("kmb"));

                                // status
                                JSONArray stats = data.getJSONArray("_stats");
                                JSONObject _sts = stats.getJSONObject(0);
                                _sumakt.setText(_sts.getString("aktif"));
                                _sumbku.setText(_sts.getString("beku"));
                                _sumcbt.setText(_sts.getString("tutup"));

                                // kat usaha
                                JSONArray skats = data.getJSONArray("_skats");
                                JSONObject skts = skats.getJSONObject(0);
                                int tskat = skts.getInt("sum");

                                JSONArray tkats = data.getJSONArray("_tkats");
                                JSONObject kats = tkats.getJSONObject(0);
                                _kat1.setText(kats.getString("nam_kref"));
                                _tkat1.setText(String.valueOf(kats.getInt("count")));
                                int xval = kats.getInt("count");

                                final int pro1 = (xval * 100) / tskat;
                                _prog1.setProgress(pro1);

                                if (tkats.length() > 1)
                                {
                                    JSONObject kat2 = tkats.getJSONObject(1);
                                    _kat2.setText(kat2.getString("nam_kref"));
                                    _tkat2.setText(String.valueOf(kat2.getInt("count")));
                                    int yval = kat2.getInt("count");

                                    final int pro2 = (yval * 100) / tskat;
                                    _prog2.setProgress(pro2);
                                } else {
                                    _kat2.setText("Data tidak tersedia");
                                    _tkat2.setText("0");
                                    _prog2.setProgress(0);
                                }

                                if (tkats.length() > 2)
                                {
                                    JSONObject kat3 = tkats.getJSONObject(2);
                                    _kat3.setText(kat3.getString("nam_kref"));
                                    _tkat3.setText(String.valueOf(kat3.getInt("count")));
                                    int zval = kat3.getInt("count");

                                    final int pro3 = (zval * 100) / tskat;
                                    _prog3.setProgress(pro3);
                                } else {
                                    _kat3.setText("Data tidak tersedia");
                                    _tkat3.setText("0");
                                    _prog3.setProgress(0);
                                }

                                // sub kat usaha
                                JSONArray sskat = data.getJSONArray("_sskat");
                                JSONObject sskt = sskat.getJSONObject(0);
                                int tssk = sskt.getInt("sum");

                                JSONArray stkat = data.getJSONArray("_tskat");
                                JSONObject stkt = stkat.getJSONObject(0);
                                _sub1.setText(stkt.getString("nam_kref"));
                                _tsub1.setText(String.valueOf(stkt.getInt("count")));
                                int val1 = stkt.getInt("count");

                                final int pro4 = (val1 * 100) / tssk;
                                _prog4.setProgress(pro4);

                                if (stkat.length() > 1)
                                {
                                    JSONObject kat5 = stkat.getJSONObject(1);
                                    _sub2.setText(kat5.getString("nam_kref"));
                                    _tsub2.setText(String.valueOf(kat5.getInt("count")));
                                    int val2 = kat5.getInt("count");

                                    final int pro5 = (val2 * 100) / tssk;
                                    _prog5.setProgress(pro5);
                                } else {
                                    _sub2.setText("Data tidak tersedia.");
                                    _tsub2.setText("0");
                                    _prog5.setProgress(0);
                                }

                                if (stkat.length() > 2)
                                {
                                    JSONObject kat6 = stkat.getJSONObject(2);
                                    _sub3.setText(kat6.getString("nam_kref"));
                                    _tsub3.setText(String.valueOf(kat6.getInt("count")));
                                    int val3 = kat6.getInt("count");

                                    final int pro6 = (val3 * 100) / tssk;
                                    _prog6.setProgress(pro6);
                                } else {
                                    _sub3.setText("Data tidak tersedia.");
                                    _tsub3.setText("0");
                                    _prog6.setProgress(0);
                                }

                                // graph
                                JSONObject graph = data.getJSONObject("_graph");

                                final int[] MY_COLORS = {Color.parseColor("#F44336"),
                                        Color.parseColor("#FFC107"),
                                        Color.parseColor("#4caf50")};
                                ArrayList<Integer> colors = new ArrayList<Integer>();
                                for(int c: MY_COLORS) colors.add(c);

                                // profil risiko
                                ArrayList<PieEntry> datSts= new ArrayList<>();
                                JSONArray grisk = graph.getJSONArray("risk");
                                for (int i = 0; i < grisk.length(); i++) {
                                    JSONObject obj = grisk.getJSONObject(i);
                                    datSts.add(new PieEntry(obj.getInt("nilai"), obj.getString("judul").toUpperCase()));
                                }
                                PieDataSet dataSet = new PieDataSet(datSts,"" );
                                dataSet.setColors(colors);
                                PieData dataSum = new PieData(dataSet);
                                dataSum.setValueFormatter(new PercentFormatter());
                                dataSum.setValueTextSize(8f);
                                dataSum.setValueTextColor(Color.BLACK);
                                _grisk.setData(dataSum);
                                _grisk.invalidate(); // refresh

                                // lokasi
                                ArrayList<PieEntry> datLok = new ArrayList<>();
                                JSONArray gloka = graph.getJSONArray("loka");
                                for (int i = 0; i < gloka.length(); i++) {
                                    JSONObject obj = gloka.getJSONObject(i);
                                    datLok.add(new PieEntry(obj.getInt("nilai"), obj.getString("judul").toUpperCase()));
                                }
                                PieDataSet dataLok = new PieDataSet(datLok,"" );
                                dataLok.setColors(ColorTemplate.LIBERTY_COLORS);
                                PieData dataPos= new PieData(dataLok);
                                dataPos.setValueFormatter(new PercentFormatter());
                                dataPos.setValueTextSize(8f);
                                dataPos.setValueTextColor(Color.BLACK);
                                _gloka.setData(dataPos);
                                _gloka.invalidate(); // refresh

                                // it inventory
                                ArrayList<PieEntry> datItv = new ArrayList<>();
                                JSONArray gitiv = graph.getJSONArray("itiv");
                                for (int i = 0; i < gitiv.length(); i++) {
                                    JSONObject obj = gitiv.getJSONObject(i);
                                    datItv.add(new PieEntry(obj.getInt("nilai"), obj.getString("judul").toUpperCase()));
                                }
                                PieDataSet dataItv = new PieDataSet(datItv,"" );
                                dataItv.setColors(ColorTemplate.LIBERTY_COLORS);
                                PieData dataIti= new PieData(dataItv);
                                dataIti.setValueFormatter(new PercentFormatter());
                                dataIti.setValueTextSize(8f);
                                dataIti.setValueTextColor(Color.BLACK);
                                _gitiv.setData(dataIti);
                                _gitiv.invalidate(); // refresh

                                // negara pic
                                List<BarEntry> datCnt = new ArrayList<>();
                                ArrayList<String> lbl = new ArrayList<>();
                                JSONArray negara = graph.getJSONArray("cntr");
                                for (int i = 0; i < negara.length(); i++) {
                                    JSONObject cnt = negara.getJSONObject(i);
                                    datCnt.add(new BarEntry(i, cnt.getInt("count")));
                                    lbl.add(cnt.getString("kat_kref"));
                                }

                                BarDataSet dataCnt = new BarDataSet(datCnt,"");
                                dataCnt.setColors(ColorTemplate.LIBERTY_COLORS);
                                BarData dataNgr = new BarData(dataCnt);
                                _gcntr.getXAxis().setValueFormatter(new IndexAxisValueFormatter(lbl));
                                _gcntr.getAxisLeft().setTextColor(Color.WHITE);
                                _gcntr.getAxisRight().setEnabled(false);
                                _gcntr.getXAxis().setDrawGridLines(false);
                                _gcntr.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
                                _gcntr.getXAxis().setTextColor(Color.WHITE);
                                _gcntr.getXAxis().setGranularity(1f);
                                _gcntr.setFitBars(true);
                                _gcntr.setData(dataNgr);
                                _gcntr.getBarData().setValueTextColor(Color.WHITE);
                                _gcntr.getBarData().setValueTextSize(8f);
                                _gcntr.getDescription().setEnabled(false);
                                _gcntr.invalidate();

                                // foul
                                List<BarEntry> datFou= new ArrayList<>();
                                ArrayList<String> lbel = new ArrayList<>();
                                JSONArray foul = graph.getJSONArray("foul");
                                for (int i = 0; i < foul.length(); i++) {
                                    JSONObject fou = foul.getJSONObject(i);
                                    datFou.add(new BarEntry(i, fou.getInt("nilai")));
                                    lbel.add(fou.getString("judul"));
                                }

                                BarDataSet dataFou = new BarDataSet(datFou,"");
                                dataFou.setColors(ColorTemplate.LIBERTY_COLORS);
                                BarData dataPel= new BarData(dataFou);
                                _gfoul.getXAxis().setValueFormatter(new IndexAxisValueFormatter(lbel));
                                _gfoul.getAxisLeft().setTextColor(Color.WHITE);
                                _gfoul.getAxisRight().setEnabled(false);
                                _gfoul.getXAxis().setDrawGridLines(false);
                                _gfoul.getXAxis().setDrawLabels(true);
                                _gfoul.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
                                _gfoul.getXAxis().setTextColor(Color.WHITE);
                                _gfoul.setFitBars(true);
                                _gfoul.getXAxis().setGranularity(1f);
                                _gfoul.setData(dataPel);
                                _gfoul.getBarData().setValueTextColor(Color.WHITE);
                                _gfoul.getBarData().setValueTextSize(10f);
                                _gfoul.getDescription().setEnabled(false);
                                _gfoul.invalidate();

                            } else {
                                Toast.makeText(context, pesan, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError){
                    Toast.makeText(context, "Connection time out", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(context, "Server problem", Toast.LENGTH_SHORT).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(context, "Internet problem", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(context, "Parsing problem", Toast.LENGTH_SHORT).show();
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() {
                headers = new HashMap<String, String>();
                headers.put("x-access-token", token);
                headers.put("kantor", extHol);
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private String tglHelper(String params){
        if (params != null && !params.isEmpty() && !params.equals("null"))
        {
            String[] sourceParams = params.split("-");
            String tahun = sourceParams[0];
            String bulan = sourceParams[1];
            String tangg = sourceParams[2].substring(0,2);

            String[] WaktuParams = params.split(" ");
            String waktu = WaktuParams[0].substring(11);
            String[] detWak = waktu.split(":");
            String jam = detWak[0];
            String menit = detWak[1];
            String detik = detWak[2].substring(0,2);

            return tangg + "/" + bulan + "/" + tahun + " " + jam + ":" + menit + ":" + detik + " WIB";
        } else {
            return "-";
        }
    }
}
