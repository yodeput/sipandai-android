package com.elwilis.emsitpro.trinitymobile.livereport;

public class SlideUtils {

    String slideImageUrl;

    public String getSlideImageUrl() {
        return slideImageUrl;
    }

    public void setSlideImageUrl(String slideImageUrl) {
        this.slideImageUrl = slideImageUrl;
    }
}
