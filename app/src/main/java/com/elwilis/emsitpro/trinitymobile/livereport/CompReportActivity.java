package com.elwilis.emsitpro.trinitymobile.livereport;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.elwilis.emsitpro.trinitymobile.R;
import com.gdacciaro.iOSDialog.iOSDialog;
import com.gdacciaro.iOSDialog.iOSDialogBuilder;
import com.gdacciaro.iOSDialog.iOSDialogClickListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.rilixtech.materialfancybutton.MaterialFancyButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

public class CompReportActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    // view
    @BindView(R.id.toolbar) Toolbar _toolbar;
    @BindView(R.id.namaKpbc) TextView _namaKpbc;
    @BindView(R.id.namaComp) TextView _namaComp;
    @BindView(R.id.kordLoka) TextView _kordLoka;
    @BindView(R.id.subjReport) AutoCompleteTextView _subjRepo;
    @BindView(R.id.contReport) AutoCompleteTextView _contRepo;
    @BindView(R.id.remoImgs) AppCompatButton _remoImgs;
    @BindView(R.id.fromGale) AppCompatButton _fromGale;
    @BindView(R.id.fromCame) AppCompatButton _fromCame;
    @BindView(R.id.sendRepo) MaterialFancyButton _sendRepo;
    @BindView(R.id.gridView) InnerGridView _gridView;

    // general
    ProgressDialog pDialog;
    Bitmap bitmap;
    Intent intent;
    File file;
    Uri fileUri;
    Context context;
    Bundle bundle;

    // holder
    String apiEnd, usrNip, tokEns, seqCom, namCom, fasCom, kanCom, refKan, subRptHolder, isiRptHolder;
    ArrayList<String> filePaths = new ArrayList<String>();
    ArrayList<Spacecraft> spacecrafts;
    CustomAdapter customAdapter;
    JSONObject jsonObject;
    ArrayList<String> encodedImageList;
    ArrayList<String> imageName;
    boolean checkEmpty;
    InputStream inputStream;

    // location
    private Location mylocation;
    private GoogleApiClient googleApiClient;
    private static final String TAG = CompReportActivity.class.getSimpleName();
    private final static int REQUEST_CHECK_SETTINGS_GPS=0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS=0x2;

    public static final int RequestPermissionCode  = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comp_report);
        ButterKnife.bind(this);
        setSupportActionBar(_toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // initialize
        intent  = getIntent();
        context = CompReportActivity.this;
        pDialog = new ProgressDialog(context);
        tokEns  = "5d81392a7817e15101834c30c290c511e66d35b8";

        // get intent data
        bundle = intent.getExtras();
        assert bundle != null;
        seqCom = String.valueOf(bundle.get("seqsComp"));
        namCom = String.valueOf(bundle.get("namaComp"));
        fasCom = String.valueOf(bundle.get("fasiComp"));
        kanCom = String.valueOf(bundle.get("kantComp"));
        usrNip = String.valueOf(bundle.get("userNips"));
        refKan = String.valueOf(bundle.get("rkanComp"));
        spacecrafts = new ArrayList<>();
        jsonObject  = new JSONObject();
        encodedImageList = new ArrayList<>();
        imageName = new ArrayList<>();
        // enable location
        setUpGClient();

        // check permission akses kamera
        EnableRuntimePermissionToAccessCamera();

        // set value
        _namaKpbc.setText(String.format("%s / %s", kanCom, fasCom));
        _namaComp.setText(namCom);

        // clear image
        _remoImgs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spacecrafts.size() > 0) {
                    spacecrafts.clear();
                    _gridView.setAdapter(null);
                    customAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(context, "Tidak ada Foto tersedia", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // open image gallery
        _fromGale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isReadStoragePermissionGranted() && isWriteStoragePermissionGranted()) {
                    final int kuota;
                    kuota = 4;
                    if (spacecrafts.size() == 0) {
                        filePaths.clear();
                        FilePickerBuilder.getInstance().setMaxCount(kuota).setSelectedFiles(filePaths)
                                .setActivityTheme(R.style.AppTheme)
                                .pickPhoto(CompReportActivity.this);
                    } else if (spacecrafts.size() > 0 && spacecrafts.size() < kuota) {
                        filePaths.clear();
                        FilePickerBuilder.getInstance().setMaxCount(kuota - spacecrafts.size()).setSelectedFiles(filePaths)
                                .setActivityTheme(R.style.AppTheme)
                                .pickPhoto(CompReportActivity.this);
                    } else {
                        Toast.makeText(context, "Maksimal 4 Foto", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        _fromCame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spacecrafts.size() < 4) {
//                    intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    file = new File(Environment.getExternalStorageDirectory(), String.valueOf(System.currentTimeMillis())+ ".jpg");
//                    fileUri = Uri.fromFile(file);
//                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
//                    startActivityForResult(intent, 7);
                    file = new File(Environment.getExternalStorageDirectory(), String.valueOf(System.currentTimeMillis())+ ".jpg");
                    intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    // fileUri = Uri.fromFile(file);
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        fileUri = FileProvider.getUriForFile(context, "com.elwilis.emsitpro.trinitymobile.provider", file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                    } else {
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                    }
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    if (intent.resolveActivity(getApplicationContext().getPackageManager()) != null) {
                        startActivityForResult(intent, 7);
                    }
                } else {
                    Toast.makeText(context, "Maksimal 4 Foto", Toast.LENGTH_SHORT).show();
                }
            }
        });

        _sendRepo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkEmpty();
                if (checkEmpty) {
                    if (spacecrafts.size() == 0) {
                        new iOSDialogBuilder(CompReportActivity.this)
                                .setTitle("Konfirmasi")
                                .setSubtitle("Anda akan mengirimkan laporan tanpa lampiran foto?")
                                .setBoldPositiveLabel(true)
                                .setCancelable(false)
                                .setPositiveListener(getString(R.string.ok), new iOSDialogClickListener() {
                                    @Override
                                    public void onClick(iOSDialog dialog) {
                                        pDialog.setMessage("Mengirim Laporan...");
                                        showDialog();
                                        // send report
                                        lessRepo();
                                        dialog.dismiss();
                                    }
                                })
                                .setNegativeListener(getString(R.string.no), new iOSDialogClickListener() {
                                    @Override
                                    public void onClick(iOSDialog dialog) {
                                        dialog.dismiss();
                                    }
                                }).build().show();
                    } else {
                        // send report
                        fullRepo();
                    }
                } else {
                    Toast.makeText(context, "Subjek dan Isi Laporan tidak boleh kosong.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    // with attachment
    private void fullRepo() {
        apiEnd = Utils.urlUpload;
        pDialog.setMessage("Mengirim Laporan...");
        showDialog();
        final JSONArray namaFoto = new JSONArray();
        final JSONArray urisFoto = new JSONArray();
        ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
        for (int i = 0; i < spacecrafts.size(); i++) {
            Uri uri  = spacecrafts.get(i).getUri();
            mArrayUri.add(uri);
            Bitmap bitmap = null;
            try {
                bitmap = getBitmapFromUri(uri);
                imageName.add(spacecrafts.get(i).getName());
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                Objects.requireNonNull(bitmap).compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                encodedImageList.add(encodedImage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        for (String encoded: encodedImageList){
            urisFoto.put(encoded);
        }
        for (String imagnam: imageName) {
            namaFoto.put(imagnam);
        }

        try {
            jsonObject.put(Utils.tokEns, tokEns);
            jsonObject.put(Utils.subRep, _subjRepo.getText().toString().trim());
            jsonObject.put(Utils.conRep, _contRepo.getText().toString().trim());
            jsonObject.put(Utils.imgNam, namaFoto);
            jsonObject.put(Utils.imgFil, urisFoto);
            jsonObject.put(Utils.kanCom, refKan);
            jsonObject.put(Utils.fasCom, fasCom);
            jsonObject.put(Utils.namCom, namCom);
            jsonObject.put(Utils.usrNip, usrNip);
            jsonObject.put(Utils.usrLoc, _kordLoka.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, apiEnd, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.e("Message from server", jsonObject.toString());
                        hideDialog();
                        try {
                            String pesan = jsonObject.getString("message");
                            Toast.makeText(getApplication(), pesan, Toast.LENGTH_SHORT).show();
                            CompReportActivity.this.finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("Message from server", volleyError.toString());
                // Toast.makeText(getApplication(), volleyError.toString(), Toast.LENGTH_SHORT).show();
                if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError){
                    Toast.makeText(context, "Connection time out", Toast.LENGTH_SHORT).show();
                } else if (volleyError instanceof ServerError) {
                    Toast.makeText(context, "Server problem", Toast.LENGTH_SHORT).show();
                } else if (volleyError instanceof NetworkError) {
                    Toast.makeText(context, "Internet problem", Toast.LENGTH_SHORT).show();
                } else if (volleyError instanceof ParseError) {
                    Toast.makeText(context, "Parsing problem", Toast.LENGTH_SHORT).show();
                }
                hideDialog();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy( 200*30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor = getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = Objects.requireNonNull(parcelFileDescriptor).getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    // without attachment
    private void lessRepo() {
        apiEnd = Utils.urlUpload;
        try {
            jsonObject.put(Utils.tokEns, tokEns);
            jsonObject.put(Utils.subRep, _subjRepo.getText().toString().trim());
            jsonObject.put(Utils.conRep, _contRepo.getText().toString().trim());
            jsonObject.put(Utils.kanCom, refKan);
            jsonObject.put(Utils.fasCom, fasCom);
            jsonObject.put(Utils.namCom, namCom);
            jsonObject.put(Utils.usrNip, usrNip);
            jsonObject.put(Utils.usrLoc, _kordLoka.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, apiEnd, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.e("Message from server", jsonObject.toString());
                        hideDialog();
                        try {
                            String pesan = jsonObject.getString("message");
                            Toast.makeText(getApplication(), pesan, Toast.LENGTH_SHORT).show();
                            CompReportActivity.this.finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("Message from server", volleyError.toString());
                // Toast.makeText(getApplication(), volleyError.toString(), Toast.LENGTH_SHORT).show();
                if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError){
                    Toast.makeText(context, "Connection time out", Toast.LENGTH_SHORT).show();
                } else if (volleyError instanceof ServerError) {
                    Toast.makeText(context, "Server problem", Toast.LENGTH_SHORT).show();
                } else if (volleyError instanceof NetworkError) {
                    Toast.makeText(context, "Internet problem", Toast.LENGTH_SHORT).show();
                } else if (volleyError instanceof ParseError) {
                    Toast.makeText(context, "Parsing problem", Toast.LENGTH_SHORT).show();
                }
                hideDialog();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy( 200*30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest);

    }

    public void EnableRuntimePermissionToAccessCamera(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(CompReportActivity.this,
                Manifest.permission.CAMERA)) {
            Toast.makeText(CompReportActivity.this,"CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_SHORT).show();
        } else {
            ActivityCompat.requestPermissions(CompReportActivity.this,new String[]{Manifest.permission.CAMERA}, RequestPermissionCode);
        }
    }

    private synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        mylocation = location;
        if (mylocation != null) {
            Double latitude  = mylocation.getLatitude();
            Double longitude = mylocation.getLongitude();
            _kordLoka.setText(String.valueOf(latitude + "," + longitude));
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        checkPermissions();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Do whatever you need
        //You can display a message here
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //You can display a message here
    }

    private void getMyLocation(){
        if(googleApiClient!=null) {
            if (googleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(CompReportActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    mylocation =                     LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(3000);
                    locationRequest.setFastestInterval(3000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi
                            .requestLocationUpdates(googleApiClient, locationRequest, this);
                    PendingResult<LocationSettingsResult> result =
                            LocationServices.SettingsApi
                                    .checkLocationSettings(googleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                        @Override
                        public void onResult(LocationSettingsResult result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    int permissionLocation = ContextCompat
                                            .checkSelfPermission(CompReportActivity.this,
                                                    Manifest.permission.ACCESS_FINE_LOCATION);
                                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                        mylocation = LocationServices.FusedLocationApi
                                                .getLastLocation(googleApiClient);
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(CompReportActivity.this,
                                                REQUEST_CHECK_SETTINGS_GPS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    break;
                            }
                        }
                    });
                }
            }
        }
    }

    @Nullable
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getMyLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        finish();
                        break;
                }
                break;
            case FilePickerConst.REQUEST_CODE_PHOTO:
                if(resultCode == RESULT_OK && data != null)
                {
                    filePaths = data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA);
                    Spacecraft s;
                    try
                    {
                        for (String path:filePaths) {
                            s = new Spacecraft();
                            s.setName(path.substring(path.lastIndexOf("/")+1));
                            s.setUri(Uri.fromFile(new File(path)));
                            spacecrafts.add(s);
                        }
                        customAdapter = new CustomAdapter(this, spacecrafts);
                        _gridView.setAdapter(customAdapter);
                        Toast.makeText(CompReportActivity.this, "Total = "+String.valueOf(spacecrafts.size()), Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case (7):
                if (resultCode == RESULT_OK) {
                    Spacecraft c;
                    try {
                        c = new Spacecraft();
                        c.setName(String.valueOf(file).substring(String.valueOf(file).lastIndexOf("/")+1));
                        c.setUri(Uri.fromFile(new File(String.valueOf(file))));
                        spacecrafts.add(c);
                        customAdapter = new CustomAdapter(this, spacecrafts);
                        _gridView.setAdapter(customAdapter);
                        customAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
        }

    }

    private void checkPermissions(){
        int permissionLocation = ContextCompat.checkSelfPermission(CompReportActivity.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            }
        }else{
            getMyLocation();
        }

    }

    public boolean isReadStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted1");
                return true;
            } else {
                Log.v(TAG,"Permission is revoked1");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 3);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted1");
            return true;
        }
    }

    public boolean isWriteStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted2");
                return true;
            } else {

                Log.v(TAG,"Permission is revoked2");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted2");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {
        switch (RC) {
            case RequestPermissionCode:
                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {
                    // Toast.makeText(QuickReportActivity.this,"Permission Granted, Now your application can access CAMERA.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(CompReportActivity.this,"Permission Canceled, Now your application cannot access CAMERA.", Toast.LENGTH_SHORT).show();
                }
                int permissionLocation = ContextCompat.checkSelfPermission(CompReportActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    getMyLocation();
                }
                break;
//            case 2:
//                Log.d(TAG, "External storage2");
//                if(PResult[0]== PackageManager.PERMISSION_GRANTED){
//                    Log.v(TAG,"Permission: "+per[0]+ "was "+PResult[0]);
//                    //resume tasks needing this permission
//                } else {
//                    Toast.makeText(CompReportActivity.this,"Permission Canceled.", Toast.LENGTH_SHORT).show();
//                }
//                break;
//            case 3:
//                Log.d(TAG, "External storage1");
//                if(PResult[0]== PackageManager.PERMISSION_GRANTED){
//                    Log.v(TAG,"Permission: "+per[0]+ "was "+PResult[0]);
//                    //resume tasks needing this permission
//                }else{
//                    Toast.makeText(CompReportActivity.this,"Permission Canceled.", Toast.LENGTH_SHORT).show();
//                }
//                break;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void checkEmpty(){
        subRptHolder = _subjRepo.getText().toString().trim();
        isiRptHolder = _contRepo.getText().toString().trim();
        checkEmpty = !(TextUtils.isEmpty(subRptHolder) || TextUtils.isEmpty(isiRptHolder));
    }

    // show dialog method
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    // hide dialog method
    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}