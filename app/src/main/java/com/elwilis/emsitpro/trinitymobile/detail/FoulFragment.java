package com.elwilis.emsitpro.trinitymobile.detail;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.elwilis.emsitpro.trinitymobile.AppVar;
import com.elwilis.emsitpro.trinitymobile.R;
import com.elwilis.emsitpro.trinitymobile.SessionManage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class FoulFragment extends BottomSheetDialogFragment {

    SessionManage _session;
    Context context;
    private BottomSheetBehavior mBehavior;
    ProgressDialog pDialog;
    String _apiend;

    // holder
    String _token;
    NumberFormat _numfor;
    Map<String, String> headers;

    // table
    TableRow tr_body;
    TextView tr_view, tr_kema, tr_urai;

    @BindView(R.id.namaKpbc) TextView _namKan;
    @BindView(R.id.namaComp) TextView _namCom;
    @BindView(R.id.nom_sbp) TextView _nomSbp;
    @BindView(R.id.tgl_sbp) TextView _tglSbp;
    @BindView(R.id.jen_kom) TextView _jenKom;
    @BindView(R.id.fou_sts) TextView _fouSts;
    @BindView(R.id.tgl_pdk) TextView _tglPdk;
    @BindView(R.id.lok_pdk) TextView _lokPdk;
    @BindView(R.id.pen_pdk) TextView _penPdk;
    @BindView(R.id.pel_pdk) TextView _pelPdk;
    @BindView(R.id.kod_kas) TextView _kodKas;
    @BindView(R.id.ket_plg) TextView _ketPlg;
    @BindView(R.id.ura_mod) TextView _uraMod;
    @BindView(R.id.per_nil) TextView _perNil;
    @BindView(R.id.pot_kur) TextView _potKur;
    @BindView(R.id.sts_tdk) TextView _stsTdk;
    @BindView(R.id.nom_tdl) TextView _nomTdl;
    @BindView(R.id.tgl_tdl) TextView _tglTdl;
    @BindView(R.id.fou_ket) TextView _fouKet;
    @BindView(R.id.tgl_inp) TextView _tglInp;
    @BindView(R.id.val_iku) TextView _valIku;
    @BindView(R.id.tbl_cnt) TableLayout _tblCnt;

    public FoulFragment() {
        // Required empty public constructor
    }

    public static FoulFragment newInstance() {
        return new FoulFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("ResourceAsColor")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        View view = View.inflate(getContext(), R.layout.foul_detail, null);

        ButterKnife.bind(this, view);

        Locale locale = new Locale("en","EN");
        _session = new SessionManage(getActivity().getApplicationContext());
        HashMap<String, String> user = _session.getUserDetails();
        _token  = user.get(SessionManage.KEY_NAME);
        _numfor = NumberFormat.getNumberInstance(locale);

        LinearLayout linearLayout = view.findViewById(R.id.rootFoul);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) linearLayout.getLayoutParams();
        params.height = getScreenHeight();
        linearLayout.setLayoutParams(params);

        Bundle mArgs = getArguments();
        String nomSbp = mArgs.getString("nom_sbp");
        String comFou = mArgs.getString("com_fou");

        jsonData(nomSbp, comFou);

        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Mohon tunggu...");
        showDialog();

        dialog.setContentView(view);
        mBehavior = BottomSheetBehavior.from((View) view.getParent());
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public void jsonData(final String nomsbp, final String comfou) {
        _apiend = AppVar.FOUL_DT;
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                _apiend,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideDialog();
                        try {
                            Boolean sukses = response.getBoolean("success");
                            String pesan = response.getString("message");

                            if (sukses) {
                                JSONObject data  = response.getJSONObject("data");
                                // common
                                JSONObject result = data.getJSONObject("_result");
                                _namKan.setText(result.getString("kantor"));
                                _namCom.setText(comfou);
                                _nomSbp.setText(result.getString("nomsbp"));
                                _tglSbp.setText(tglOnly(result.getString("tglsbp")));
                                _jenKom.setText(result.getString("kodpnd"));
                                _fouSts.setText(cekNull(result.getString("stsfou")));
                                _tglPdk.setText(tglOnly(result.getString("tgltdk")));
                                _lokPdk.setText(result.getString("loktdk"));
                                _penPdk.setText(result.getString("pictdk"));
                                _pelPdk.setText(result.getString("tskpdk"));
                                _kodKas.setText(result.getString("kodkas"));
                                _ketPlg.setText(cekNull(result.getString("ketfou")));
                                _uraMod.setText(cekNull(result.getString("ketmod")));
                                _perNil.setText(cekNull(result.getString("estnil")));
                                _potKur.setText(cekNull(result.getString("potmin")));
                                _stsTdk.setText(cekNull(result.getString("stsfol")));
                                _nomTdl.setText(cekNull(result.getString("nomfol")));
                                _tglTdl.setText(tglOnly(result.getString("tglfol")));
                                _fouKet.setText(cekNull(result.getString("ketlai")));
                                _tglInp.setText(tglOnly(result.getString("tglinp")));
                                _valIku.setText(cekNull(result.getString("ketiku")));
                                // table
                                JSONArray detail = data.getJSONArray("_detail");
                                for (int i = 0; i < detail.length(); i++ ){
                                    JSONObject obj = detail.getJSONObject(i);

                                    tr_body = new TableRow(getActivity().getApplicationContext());
                                    tr_body.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));

                                    tr_view = new TextView(getActivity().getApplicationContext());
                                    tr_view.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
                                    tr_view.setText(obj.getString("jumpnd"));
                                    tr_view.setPaddingRelative(30,10,10,10);
                                    tr_body.addView(tr_view);

                                    tr_kema = new TextView(getActivity().getApplicationContext());
                                    tr_kema.setText(obj.getString("kmspnd"));
                                    tr_kema.setPaddingRelative(30,10,10,10);
                                    tr_body.addView(tr_kema);

                                    tr_urai= new TextView(getActivity().getApplicationContext());
                                    tr_urai.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
                                    tr_urai.setText(obj.getString("detpnd"));
                                    tr_urai.setPaddingRelative(30,10,10,10);
                                    tr_body.addView(tr_urai);

                                    _tblCnt.addView(tr_body);
                                }
                            } else {
                                Toast.makeText(getActivity().getApplicationContext(), pesan, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError){
                    Toast.makeText(getActivity().getApplicationContext(), "Connection time out", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(getActivity().getApplicationContext(), "Server problem", Toast.LENGTH_SHORT).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(getActivity().getApplicationContext(), "Internet problem", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(getActivity().getApplicationContext(), "Parsing problem", Toast.LENGTH_SHORT).show();
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() {
                headers = new HashMap<String, String>();
                headers.put("x-access-token", _token);
                headers.put("x-params-nomsbp", nomsbp);
                return headers;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private String tglHelper(String params){
        if (params != null && !params.isEmpty() && params != "null")
        {
            String[] sourceParams = params.split("-");
            String tahun = sourceParams[0];
            String bulan = sourceParams[1];
            String tangg = sourceParams[2].substring(0,2);

            String[] WaktuParams = params.split(" ");
            String waktu = WaktuParams[0].substring(11);
            String[] detWak = waktu.split(":");
            String jam = detWak[0];
            String menit = detWak[1];
            String detik = detWak[2].substring(0,2);

            return tangg + "/" + bulan + "/" + tahun + " " + jam + ":" + menit + ":" + detik + " WIB";
        } else {
            return "-";
        }
    }

    private String tglOnly(String params){
        if (params != null && !params.isEmpty() && params != "null")
        {
            String[] sourceParams = params.split("-");
            String tahun = sourceParams[0];
            String bulan = sourceParams[1];
            String tangg = sourceParams[2].substring(0,2);
            return tangg + "/" + bulan + "/" + tahun;
        } else {
            return "-";
        }
    }

    private String cekNull(String params){
        if (params != null && !params.equals("") && !params.equals("null")){
            return params;
        } else {
            return "-";
        }
    }

}