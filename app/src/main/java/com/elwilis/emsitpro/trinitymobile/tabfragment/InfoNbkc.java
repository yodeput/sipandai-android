package com.elwilis.emsitpro.trinitymobile.tabfragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.elwilis.emsitpro.trinitymobile.AppVar;
import com.elwilis.emsitpro.trinitymobile.R;
import com.elwilis.emsitpro.trinitymobile.SessionManage;
import com.elwilis.emsitpro.trinitymobile.SkepActivity;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InfoNbkc extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar _toolbar;
    @BindView(R.id.ptTitle) TextView _gbtitle;
    @BindView(R.id.ptLast) TextView _gblast;
    @BindView(R.id.item_counter) TextView _sumpt;
    @BindView(R.id.item_counter2) TextView _sumpbr;
    @BindView(R.id.item_counter4) TextView _sumtmp;
    @BindView(R.id.item_counter5) TextView _sumtpe;
    @BindView(R.id.item_counter6) TextView _sumpny;
    @BindView(R.id.infoKbAktif) TextView _sumakt;
    @BindView(R.id.infoKbBeku) TextView _sumbku;
    @BindView(R.id.infoKbCabut) TextView _sumcbt;
    @BindView(R.id.pieJns) PieChart _gjnis;
    @BindView(R.id.pieJnu) PieChart _gjnus;
    @BindView(R.id.barFou) BarChart _gfoul;

    // general
    Context context;
    SessionManage session;
    ProgressDialog pDialog;
    NumberFormat numFor;
    Locale locale;
    Intent intent;
    Bundle bundle;

    // holder & adapter
    String apiEnd, token, formatDateTime, extHol;
    int skpVal0, skpVal1, skpVal2;
    Map<String, String> headers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_nbkc);
        ButterKnife.bind(this);
        setSupportActionBar(_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        context = InfoNbkc.this;
        locale  = new Locale("en","EN");
        session = new SessionManage(context);

        HashMap<String, String> user = session.getUserDetails();
        token   = user.get(SessionManage.KEY_NAME);

        numFor  = NumberFormat.getNumberInstance(locale);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Mohon tunggu...");
        showDialog();

        if (savedInstanceState == null) {
            bundle = getIntent().getExtras();
            if (bundle == null) {
                extHol = "";
            } else {
                extHol = bundle.getString("refkan");
            }
        }

        // jenis bkc
        _gjnis.setUsePercentValues(true);
        _gjnis.getDescription().setEnabled(false);
        _gjnis.setDrawHoleEnabled(true);
        _gjnis.setHoleColor(Color.parseColor("#2E3858"));
        _gjnis.setTransparentCircleColor(Color.WHITE);
        _gjnis.setTransparentCircleAlpha(110);
        _gjnis.setHoleRadius(48f);
        _gjnis.setTransparentCircleRadius(50f);
        _gjnis.setDrawCenterText(true);
        _gjnis.setRotationAngle(0);
        _gjnis.setRotationEnabled(true);
        _gjnis.setHighlightPerTapEnabled(true);
        _gjnis.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        _gjnis.setDrawEntryLabels(false);

        Legend jnisLeg = _gjnis.getLegend();
        jnisLeg.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        jnisLeg.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        jnisLeg.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        jnisLeg.setDrawInside(false);
        jnisLeg.setXEntrySpace(10f);
        jnisLeg.setYEntrySpace(0f);
        jnisLeg.setYOffset(5f);
        jnisLeg.setTextColor(Color.WHITE);

        // jnus
        _gjnus.setUsePercentValues(true);
        _gjnus.getDescription().setEnabled(false);
        _gjnus.setDrawHoleEnabled(true);
        _gjnus.setHoleColor(Color.parseColor("#2E3858"));
        _gjnus.setTransparentCircleColor(Color.WHITE);
        _gjnus.setTransparentCircleAlpha(110);
        _gjnus.setHoleRadius(48f);
        _gjnus.setTransparentCircleRadius(50f);
        _gjnus.setDrawCenterText(true);
        _gjnus.setRotationAngle(0);
        _gjnus.setRotationEnabled(true);
        _gjnus.setHighlightPerTapEnabled(true);
        _gjnus.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        _gjnus.setDrawEntryLabels(false);

        Legend jnusLeg = _gjnus.getLegend();
        jnusLeg.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        jnusLeg.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        jnusLeg.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        jnusLeg.setDrawInside(false);
        jnusLeg.setXEntrySpace(10f);
        jnusLeg.setYEntrySpace(0f);
        jnusLeg.setYOffset(5f);
        jnusLeg.setTextColor(Color.WHITE);

        // foul
        Legend foulLeg = _gfoul.getLegend();
        foulLeg.setEnabled(false);

        jsonData(extHol);

    }

    private void jsonData(final String extHol) {
        if (!"050000".equals(extHol)) {
            apiEnd = AppVar.BCKC_URL;
        } else {
            apiEnd = AppVar.KWKC_URL;
        }
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                apiEnd,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideDialog();
                        try {
                            Boolean sukses = response.getBoolean("success");
                            String pesan = response.getString("message");
                            if (sukses) {
                                JSONObject data = response.getJSONObject("data");

                                // header
                                JSONArray lastu = data.getJSONArray("_lastu");
                                JSONObject _data = lastu.getJSONObject(0);
                                _gbtitle.setText(_data.getString("kant"));
                                final String vDate = _data.getString("last");

                                Instant instant = null;
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                                    instant = Instant.parse(vDate);
                                    LocalDateTime result = LocalDateTime.ofInstant(instant, ZoneId.of(ZoneOffset.UTC.getId()));
                                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
                                    formatDateTime = "Diperbarui tanggal: " + result.format(formatter);
                                } else {
                                    formatDateTime = "Diperbarui tanggal: " + tglHelper(vDate);
                                }
                                _gblast.setText(formatDateTime);

                                // sum
                                JSONArray jumla = data.getJSONArray("_jumla");
                                JSONObject _jum = jumla.getJSONObject(0);
                                _sumpt.setText(_jum.getString("total"));
                                _sumpbr.setText(_jum.getString("pab"));
                                _sumtmp.setText(_jum.getString("sto"));
                                _sumtpe.setText(_jum.getString("tpe"));
                                _sumpny.setText(_jum.getString("dis"));

                                // status
                                JSONArray stats = data.getJSONArray("_stats");
                                JSONObject _sts = stats.getJSONObject(0);
                                _sumakt.setText(_sts.getString("aktif"));
                                _sumbku.setText(_sts.getString("beku"));
                                _sumcbt.setText(_sts.getString("tutup"));

                                // graph
                                JSONObject graph = data.getJSONObject("_graph");

                                final int[] MY_COLORS = {Color.parseColor("#F44336"),
                                        Color.parseColor("#FFC107"),
                                        Color.parseColor("#4caf50")};
                                ArrayList<Integer> colors = new ArrayList<Integer>();
                                for(int c: MY_COLORS) colors.add(c);

                                // jenis bkc
                                ArrayList<PieEntry> datLok = new ArrayList<>();
                                JSONArray gloka = graph.getJSONArray("jbkc");
                                for (int i = 0; i < gloka.length(); i++) {
                                    JSONObject obj = gloka.getJSONObject(i);
                                    datLok.add(new PieEntry(obj.getInt("nilai"), obj.getString("judul").toUpperCase()));
                                }
                                PieDataSet dataLok = new PieDataSet(datLok,"" );
                                dataLok.setColors(ColorTemplate.LIBERTY_COLORS);
                                PieData dataPos= new PieData(dataLok);
                                dataPos.setValueFormatter(new PercentFormatter());
                                dataPos.setValueTextSize(8f);
                                dataPos.setValueTextColor(Color.BLACK);
                                _gjnis.setData(dataPos);
                                _gjnis.invalidate(); // refresh

                                // jenis usaha
                                ArrayList<PieEntry> datCct = new ArrayList<>();
                                JSONArray gcctv = graph.getJSONArray("baus");
                                for (int i = 0; i < gcctv.length(); i++) {
                                    JSONObject obj = gcctv.getJSONObject(i);
                                    datCct.add(new PieEntry(obj.getInt("nilai"), obj.getString("judul").toUpperCase()));
                                }
                                PieDataSet dataCct= new PieDataSet(datCct,"" );
                                dataCct.setColors(ColorTemplate.LIBERTY_COLORS);
                                PieData dataCtv = new PieData(dataCct);
                                dataCtv.setValueFormatter(new PercentFormatter());
                                dataCtv.setValueTextSize(8f);
                                dataCtv.setValueTextColor(Color.BLACK);
                                _gjnus.setData(dataCtv);
                                _gjnus.invalidate(); // refresh

                                // foul
                                List<BarEntry> datFou= new ArrayList<>();
                                ArrayList<String> lbel = new ArrayList<>();
                                JSONArray foul = graph.getJSONArray("foul");
                                for (int i = 0; i < foul.length(); i++) {
                                    JSONObject fou = foul.getJSONObject(i);
                                    datFou.add(new BarEntry(i, fou.getInt("nilai")));
                                    lbel.add(fou.getString("judul"));
                                }

                                BarDataSet dataFou = new BarDataSet(datFou,"");
                                dataFou.setColors(ColorTemplate.LIBERTY_COLORS);
                                BarData dataPel= new BarData(dataFou);
                                _gfoul.getXAxis().setValueFormatter(new IndexAxisValueFormatter(lbel));
                                _gfoul.getAxisLeft().setTextColor(Color.WHITE);
                                _gfoul.getAxisRight().setEnabled(false);
                                _gfoul.getXAxis().setDrawGridLines(false);
                                _gfoul.getXAxis().setDrawLabels(true);
                                _gfoul.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
                                _gfoul.getXAxis().setTextColor(Color.WHITE);
                                _gfoul.setFitBars(true);
                                _gfoul.getXAxis().setGranularity(1f);
                                _gfoul.setData(dataPel);
                                _gfoul.getBarData().setValueTextColor(Color.WHITE);
                                _gfoul.getBarData().setValueTextSize(10f);
                                _gfoul.getDescription().setEnabled(false);
                                _gfoul.invalidate();

                            } else {
                                Toast.makeText(context, pesan, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError){
                    Toast.makeText(context, "Connection time out", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(context, "Server problem", Toast.LENGTH_SHORT).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(context, "Internet problem", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(context, "Parsing problem", Toast.LENGTH_SHORT).show();
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() {
                headers = new HashMap<String, String>();
                headers.put("x-access-token", token);
                headers.put("kantor", extHol);
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private String tglHelper(String params){
        if (params != null && !params.isEmpty() && !params.equals("null"))
        {
            String[] sourceParams = params.split("-");
            String tahun = sourceParams[0];
            String bulan = sourceParams[1];
            String tangg = sourceParams[2].substring(0,2);

            String[] WaktuParams = params.split(" ");
            String waktu = WaktuParams[0].substring(11);
            String[] detWak = waktu.split(":");
            String jam = detWak[0];
            String menit = detWak[1];
            String detik = detWak[2].substring(0,2);

            return tangg + "/" + bulan + "/" + tahun + " " + jam + ":" + menit + ":" + detik + " WIB";
        } else {
            return "-";
        }
    }
}
