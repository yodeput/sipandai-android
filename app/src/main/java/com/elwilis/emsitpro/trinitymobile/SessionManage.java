package com.elwilis.emsitpro.trinitymobile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;

/**
 * Created by rezar on 23/01/18.
 */

public class SessionManage {
    // shared pref
    SharedPreferences pref;

    // editor shared pref
    Editor editor;

    // context
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    private static final String PREF_NAME   = "TrinityPref";
    private static final String IS_LOGIN    = "IsLoggedIn";
    public static final String KEY_NAME     = "token";
    public static final String KEY_NIP      = "nip";
    public static final String KEY_MAIL     = "email";
    public static final String KEY_HASH     = "ucode";
    public static final String KEY_UNAME    = "nam";
    public static final String KEY_RAWP     = "rawpass";
    public static final String KEY_GRUP     = "grup";
    public static final String KEY_KPBC     = "kpbc";

    public SessionManage(Context context)
    {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLoginSession(String nip, String token, String nam, String email, String rawpass, String grup, String kpbc)
    {
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_NAME, token);
        editor.putString(KEY_NIP, nip);
        editor.putString(KEY_UNAME, nam);
        editor.putString(KEY_MAIL, email);
        editor.putString(KEY_RAWP, rawpass);
        editor.putString(KEY_GRUP, grup);
        editor.putString(KEY_KPBC, kpbc);
        editor.commit();
    }

    public void forgotPwdSession(String email, String ucode) {
        editor.putString(KEY_MAIL, email);
        editor.putString(KEY_HASH, ucode);
        editor.commit();
    }

    public void checkLogin(){
        if (!this.isLoggedIn()) {

            Intent i = new Intent(_context, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);
        }
    }

    // get stored session data
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_NAME, pref.getString(KEY_NAME, null));
        user.put(KEY_NIP, pref.getString(KEY_NIP, null));
        user.put(KEY_UNAME, pref.getString(KEY_UNAME, null));
        user.put(KEY_MAIL, pref.getString(KEY_MAIL, null));
        user.put(KEY_GRUP, pref.getString(KEY_GRUP, null));
        user.put(KEY_KPBC, pref.getString(KEY_KPBC, null));
        return user;
    }

    // get stored forgot session data
    public HashMap<String, String> getForgotPwd() {
        HashMap<String, String> forgot = new HashMap<>();
        forgot.put(KEY_MAIL, pref.getString(KEY_MAIL,null));
        forgot.put(KEY_HASH, pref.getString(KEY_HASH, null));
        return forgot;
    }

    public void logoutUser(){
        editor.clear();
        editor.commit();
        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, MainActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        // Staring Login Activity
        _context.startActivity(i);
    }

    // get login state
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }

}
