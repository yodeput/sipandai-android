package com.elwilis.emsitpro.trinitymobile;

/**
 * Created by rezar on 30/01/18.
 */

public class DataObject {

    // common field holder
    private int seqFasi;
    private String kodKant;
    private String namComp;
    private String refBaus;
    private String alaComp;
    private String refRisk;
    private String updDate;
    private String gpsLoca;
    private String refPoin;

    public DataObject(String kodKant, String namComp, String alaComp, String refRisk, String updDate, String gpsLoca, String refPoin) {
        this.kodKant = kodKant;
        this.namComp = namComp;
        this.alaComp = alaComp;
        this.refRisk = refRisk;
        this.updDate = updDate;
        this.gpsLoca = gpsLoca;
        this.refPoin = refPoin;
    }

    public int getSeqFasi() {
        return seqFasi;
    }
    public String getKodKant() {
        return kodKant;
    }
    public String getNamComp() {
        return namComp;
    }
    public String getAlaComp() { return alaComp; }
    public String getRefRisk() {
        return refRisk;
    }
    public String getUpdDate() { return updDate; }
    public String getGpsLoca() { return gpsLoca; }
    public String getRefPoin() {
        return refPoin;
    }

}
