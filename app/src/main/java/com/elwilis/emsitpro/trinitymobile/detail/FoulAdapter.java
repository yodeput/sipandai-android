package com.elwilis.emsitpro.trinitymobile.detail;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.elwilis.emsitpro.trinitymobile.ItemClickListener;
import com.elwilis.emsitpro.trinitymobile.R;

import java.util.List;

public class FoulAdapter extends RecyclerView.Adapter<FoulHolder> {

    private List<FoulObject> foulObj;
    private Context context;
    private LayoutInflater inflater;
    Intent intent;
    private FragmentManager fragmentManager;

    public FoulAdapter(Context context, List<FoulObject> foulList, android.support.v4.app.FragmentManager supportFragmentManager) {
        this.context = context;
        this.foulObj = foulList;
        fragmentManager = supportFragmentManager;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public FoulHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = inflater.inflate(R.layout.card_foul_holder, parent, false);
        return new FoulHolder(rootView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull FoulHolder holder, final int i) {
        final FoulObject foulObject = foulObj.get(i);

        holder.stsFoul.setText(foulObject.getStsFoul());
        holder.nomFoul.setText("NO. " + foulObject.getNomFoul() + " - TGL. " + foulObject.getTglFoul());
        holder.kpbFoul.setText(foulObject.getKpbFoul());
        holder.refFoul.setText(foulObject.getRefFoul());
        holder.comFoul.setText(foulObject.getComFoul());

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View rootView, int i) {
                // slide panel up
                Bundle args = new Bundle();
                args.putString("nom_sbp", foulObj.get(i).getNomFoul());
                args.putString("com_fou", foulObj.get(i).getComFoul());

                FoulFragment foulFragment = FoulFragment.newInstance();
                foulFragment.setArguments(args);
                foulFragment.show(fragmentManager, foulFragment.getTag());
            }
        });
    }

    @Override
    public int getItemCount() {
        return foulObj.size();
    }

}