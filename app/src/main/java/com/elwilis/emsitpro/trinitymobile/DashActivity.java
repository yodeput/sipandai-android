package com.elwilis.emsitpro.trinitymobile;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.elwilis.emsitpro.trinitymobile.bcbandung.BdgActivity;
import com.elwilis.emsitpro.trinitymobile.bcbekasi.BksActivity;
import com.elwilis.emsitpro.trinitymobile.bcbogor.BgrActivity;
import com.elwilis.emsitpro.trinitymobile.bccikarang.CkrActivity;
import com.elwilis.emsitpro.trinitymobile.bccirebon.CrbActivity;
import com.elwilis.emsitpro.trinitymobile.bcpurwakarta.PwkActivity;
import com.elwilis.emsitpro.trinitymobile.bctasik.TskActivity;
import com.elwilis.emsitpro.trinitymobile.locationtracker.TracLocService;
import com.elwilis.emsitpro.trinitymobile.tabfragment.TabPager;
import com.gdacciaro.iOSDialog.iOSDialog;
import com.gdacciaro.iOSDialog.iOSDialogBuilder;
import com.gdacciaro.iOSDialog.iOSDialogClickListener;

import org.apache.commons.text.WordUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class DashActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, TabLayout.OnTabSelectedListener {

    Context _context;
    DrawerLayout _drawer;
    NavigationView _naview;
    SessionManage _session;
    TabLayout _tabbed;
    ViewPager _viepag;
    Intent intent;

    String namHol, emaHol, tokHol, nipHol, pasHol, apiEnd, grpHol, kpbHol;
    String versionName = "";
    int versionCode = -1;

    public static final String EXTRA_MESSAGE = "com.elwilis.emsitpro.trinitymobile.message";
    private boolean startedService = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash);

        _session = new SessionManage(getApplicationContext());
        _session.checkLogin();
        _context = DashActivity.this;
        intent   = new Intent(this, TracLocService.class);

        HashMap<String, String> user = _session.getUserDetails();
        namHol = user.get(SessionManage.KEY_UNAME);
        emaHol = user.get(SessionManage.KEY_MAIL);
        tokHol = user.get(SessionManage.KEY_NAME);
        nipHol = user.get(SessionManage.KEY_NIP);
        pasHol = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        grpHol = user.get(SessionManage.KEY_GRUP);
        kpbHol = user.get(SessionManage.KEY_KPBC);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        _naview = findViewById(R.id.nav_view);
        _naview.setNavigationItemSelectedListener(this);
        _naview.getMenu().getItem(0).setChecked(true);
        View header = _naview.getHeaderView(0);

        final TextView namHolder = header.findViewById(R.id.user_name);
        final TextView emaHolder = header.findViewById(R.id.user_mail);
        namHolder.setText(WordUtils.capitalize(namHol));
        emaHolder.setText(emaHol);

        _tabbed = findViewById(R.id.tabLayout);
        _tabbed.addTab(_tabbed.newTab().setText("Infografis"));
        _tabbed.addTab(_tabbed.newTab().setText("Laporan"));
        _tabbed.setTabGravity(TabLayout.GRAVITY_FILL);
        _tabbed.addOnTabSelectedListener(this);

        _viepag = findViewById(R.id.pager);
        TabPager adapter = new TabPager(getSupportFragmentManager(), _tabbed.getTabCount());
        _viepag.setAdapter(adapter);
        _viepag.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(_tabbed));

        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;
            versionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        final String fbtoken = SharedPrefManager.getInstance(this).getDeviceToken();
        if (fbtoken != null) { registerToken(fbtoken); }
    }

    private void registerToken(final String fbtoken){
        apiEnd = AppVar.USER_FBT;
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                apiEnd,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject rsp = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError || error instanceof NoConnectionError){
                            Toast.makeText(_context, "Connection time out", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(_context, "Server problem", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(_context, "Internet problem", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(_context, "Parsing problem", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        ){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("usernip", nipHol);
                params.put("usertkn", fbtoken);
                return params;
            }
            @Override
            public Map<String,String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        _viepag.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {}

    @Override
    public void onTabReselected(TabLayout.Tab tab) {}

    @Override
    public void onBackPressed() { Toast.makeText(getApplicationContext(), "Gunakan menu Keluar Aplikasi.", Toast.LENGTH_SHORT).show(); }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (item.isChecked()){
            _drawer.closeDrawer(GravityCompat.START);
            return false;
        }
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    private void displaySelectedScreen(int itemId){

        if (itemId == R.id.nav_bgr) {
            // user grup validation
            if (kpbHol.equals("050000") || kpbHol.equals("050300"))
            {
                Intent intent = new Intent(_context, BgrActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } else {
                new iOSDialogBuilder(_context)
                        .setTitle("Peringatan")
                        .setSubtitle("Anda tidak memiliki hak akses halaman ini.")
                        .setBoldPositiveLabel(true)
                        .setCancelable(false)
                        .setPositiveListener(getString(R.string.ok),new iOSDialogClickListener() {
                            @Override
                            public void onClick(iOSDialog dialog) {
                                dialog.dismiss();
                            }
                        }).build().show();
            }
        } else if (itemId == R.id.nav_bdg) {
            if (kpbHol.equals("050000") || kpbHol.equals("050500"))
            {
                Intent intent = new Intent(_context, BdgActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } else {
                new iOSDialogBuilder(_context)
                        .setTitle("Peringatan")
                        .setSubtitle("Anda tidak memiliki hak akses halaman ini.")
                        .setBoldPositiveLabel(true)
                        .setCancelable(false)
                        .setPositiveListener(getString(R.string.ok),new iOSDialogClickListener() {
                            @Override
                            public void onClick(iOSDialog dialog) {
                                dialog.dismiss();
                            }
                        }).build().show();
            }
        } else if (itemId == R.id.nav_tsk) {
            if (kpbHol.equals("050000") || kpbHol.equals("050600"))
            {
                Intent intent = new Intent(_context, TskActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } else {
                new iOSDialogBuilder(_context)
                        .setTitle("Peringatan")
                        .setSubtitle("Anda tidak memiliki hak akses halaman ini.")
                        .setBoldPositiveLabel(true)
                        .setCancelable(false)
                        .setPositiveListener(getString(R.string.ok),new iOSDialogClickListener() {
                            @Override
                            public void onClick(iOSDialog dialog) {
                                dialog.dismiss();
                            }
                        }).build().show();
            }
        } else if (itemId == R.id.nav_crb) {
            if (kpbHol.equals("050000") || kpbHol.equals("050700"))
            {
                Intent intent = new Intent(_context, CrbActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } else {
                new iOSDialogBuilder(_context)
                        .setTitle("Peringatan")
                        .setSubtitle("Anda tidak memiliki hak akses halaman ini.")
                        .setBoldPositiveLabel(true)
                        .setCancelable(false)
                        .setPositiveListener(getString(R.string.ok),new iOSDialogClickListener() {
                            @Override
                            public void onClick(iOSDialog dialog) {
                                dialog.dismiss();
                            }
                        }).build().show();
            }
        } else if (itemId == R.id.nav_pwk) {
            if (kpbHol.equals("050000") || kpbHol.equals("050800"))
            {
                Intent intent = new Intent(_context, PwkActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } else {
                new iOSDialogBuilder(_context)
                        .setTitle("Peringatan")
                        .setSubtitle("Anda tidak memiliki hak akses halaman ini.")
                        .setBoldPositiveLabel(true)
                        .setCancelable(false)
                        .setPositiveListener(getString(R.string.ok),new iOSDialogClickListener() {
                            @Override
                            public void onClick(iOSDialog dialog) {
                                dialog.dismiss();
                            }
                        }).build().show();
            }
        } else if (itemId == R.id.nav_bks) {
            if (kpbHol.equals("050000") || kpbHol.equals("050900"))
            {
                Intent intent = new Intent(_context, BksActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } else {
                new iOSDialogBuilder(_context)
                        .setTitle("Peringatan")
                        .setSubtitle("Anda tidak memiliki hak akses halaman ini.")
                        .setBoldPositiveLabel(true)
                        .setCancelable(false)
                        .setPositiveListener(getString(R.string.ok),new iOSDialogClickListener() {
                            @Override
                            public void onClick(iOSDialog dialog) {
                                dialog.dismiss();
                            }
                        }).build().show();
            }
        } else if (itemId == R.id.nav_ckr) {
            if (kpbHol.equals("050000") || kpbHol.equals("051000"))
            {
                Intent intent = new Intent(_context, CkrActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } else {
                new iOSDialogBuilder(_context)
                        .setTitle("Peringatan")
                        .setSubtitle("Anda tidak memiliki hak akses halaman ini.")
                        .setBoldPositiveLabel(true)
                        .setCancelable(false)
                        .setPositiveListener(getString(R.string.ok),new iOSDialogClickListener() {
                            @Override
                            public void onClick(iOSDialog dialog) {
                                dialog.dismiss();
                            }
                        }).build().show();
            }
        } else if (itemId == R.id.nav_abo) {
            AlertDialog alertDialog = new AlertDialog.Builder(DashActivity.this).create();
            alertDialog.setTitle("Informasi");
            alertDialog.setMessage("Aplikasi SIPANDAI v"+versionName+"\nDikembangkan oleh Elwilis\nhttps://elwilis.com - 2019");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        } else if (itemId == R.id.nav_exi) {
            _session.logoutUser();
            stopService(intent);
            startedService = false;
            Intent intent = new Intent(_context, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        displaySelectedScreen(item.getItemId());
        return true;
    }
}
