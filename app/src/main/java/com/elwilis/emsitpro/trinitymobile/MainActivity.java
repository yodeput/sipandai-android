package com.elwilis.emsitpro.trinitymobile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.rilixtech.materialfancybutton.MaterialFancyButton;

import io.fabric.sdk.android.Fabric;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import studio.carbonylgroup.textfieldboxes.ExtendedEditText;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.ext_uname) ExtendedEditText _uname;
    @BindView(R.id.ext_upass) ExtendedEditText _upass;
    @BindView(R.id.btnLogin) MaterialFancyButton _btnLogin;
    @BindView(R.id.actForpas) TextView _fpass;

    Context _context;
    ProgressDialog _dialog;
    ConnectivityManager _conman;
    SessionManage _session;

    boolean chkEmp;
    String namHol, pasHol, apiEnd;

    public static final String EXTRA_MESSAGE     = "com.elwilis.emsitpro.trinitymobile.message";
    private static final int PERMISSIONS_REQUEST = 1;
    private static String[] PERMISSIONS_REQUIRED = new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        _context = MainActivity.this;
        _conman  = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        _session = new SessionManage(getApplicationContext());
        _dialog  = new ProgressDialog(_context);
        apiEnd   = AppVar.LOGIN_URL;

        _btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkEmptyLogin();
                if (chkEmp) {
                    if (_conman.getActiveNetworkInfo() != null && _conman.getActiveNetworkInfo().isAvailable() && _conman.getActiveNetworkInfo().isConnected()) {
                        login();
                    } else {
                        Toast.makeText(_context, "Tidak ada koneksi internet.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(_context, "NIP/Email & Kata Sandi wajib diisi.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        if (!isTaskRoot()) {
            finish();
            return;
        }
    }

    // login method
    private void login() {

        final String nip        = _uname.getText().toString().trim();
        final String password   = _upass.getText().toString().trim();

        _dialog.setMessage("Proses Autentifikasi...");
        showDialog();

        RequestQueue requestQueue   = Volley.newRequestQueue(_context);
        StringRequest strReq        = new StringRequest(
                Request.Method.POST,
                apiEnd,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideDialog();
                        try {
                            JSONObject rsp  = new JSONObject(response);
                            Boolean sukses  = rsp.getBoolean("success");
                            String pesan    = rsp.getString("message");

                            if (sukses == AppVar.RESPONSE_SUCCESS){
                                JSONObject dta  = rsp.getJSONObject("data");
                                String token    = rsp.getString("token");
                                String email    = dta.getString("ema");
                                String nam      = dta.getString("nam");
                                String unip     = dta.getString("nip");
                                String grup     = dta.getString("grp");
                                String kpbc     = dta.getString("kpb");
                                _session.createLoginSession(unip, token, nam, email, password, grup, kpbc);
                                gotoDash();
                            } else {
                                Toast.makeText(MainActivity.this, pesan, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){
                        hideDialog();
                        if (error instanceof TimeoutError || error instanceof NoConnectionError){
                            Toast.makeText(_context, "Connection time out", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(_context, "Server problem", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(_context, "Internet problem", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(_context, "Parsing problem", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        ){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("usernam", nip);
                params.put("userpas", password);
                return params;
            }
            @Override
            public Map<String,String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        requestQueue.add(strReq);
    }

    // redirect to dash activity
    private void gotoDash() {
        Intent intent = new Intent(_context, DashActivity.class);
        final String userPass = _upass.getText().toString().trim();
        intent.putExtra(EXTRA_MESSAGE, userPass);
        startActivity(intent);
        finish();
    }

    // forgot password step 1
    public void forPasFirst(View v){
        Intent intent = new Intent(_context, ForgotPassStep1.class);
        startActivity(intent);
    }

    private void showDialog() {
        if (!_dialog.isShowing())
            _dialog.show();
    }

    private void hideDialog() {
        if (_dialog.isShowing())
            _dialog.dismiss();
    }

    private void checkEmptyLogin() {
        namHol = _uname.getText().toString().trim();
        pasHol = _upass.getText().toString().trim();
        chkEmp = !(TextUtils.isEmpty(namHol) || TextUtils.isEmpty(pasHol));
    }

}