package com.elwilis.emsitpro.trinitymobile.skepmodel;

/**
 * Created by rezar on 10/02/18.
 */

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.elwilis.emsitpro.trinitymobile.R;

import java.util.HashMap;
import java.util.List;

public class ExpListAdapterZero extends BaseExpandableListAdapter {

    private Context _context;
    private String _listDataHeaderZero; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChildZero;

    public ExpListAdapterZero(Context context, String _listDataHeaderZero,
                          HashMap<String, List<String>> listChildDataZero) {
        this._context = context;
        this._listDataHeaderZero = _listDataHeaderZero;
        this._listDataChildZero = listChildDataZero;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChildZero.get(this._listDataHeaderZero).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.skep_file_0, null);
        }

        TextView txtListChild = convertView.findViewById(R.id.skepItem0);

        txtListChild.setText(childText);
        if (childPosition %2 == 0) {
            txtListChild.setBackgroundColor(Color.WHITE);
        } else {
            txtListChild.setBackgroundColor(Color.parseColor("#FAFAFA"));
        }
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChildZero.get(this._listDataHeaderZero).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeaderZero;
    }

    @Override
    public int getGroupCount() {
        return 1;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_skep_0, null);
        }

        TextView lblListHeader = convertView.findViewById(R.id.skepGroup0);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
