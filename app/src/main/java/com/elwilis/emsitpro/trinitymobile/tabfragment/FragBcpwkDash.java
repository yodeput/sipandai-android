package com.elwilis.emsitpro.trinitymobile.tabfragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.elwilis.emsitpro.trinitymobile.AppVar;
import com.elwilis.emsitpro.trinitymobile.R;
import com.elwilis.emsitpro.trinitymobile.SessionManage;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragBcpwkDash extends Fragment {

    // general
    SessionManage _session;
    ProgressDialog _dialog;
    String _apiend;
    Intent intent;

    // holder
    String _token;
    NumberFormat _numfor;
    Map<String, String> headers;

    // bindview
    @BindView(R.id.badgeKb) TextView _countkb;
    @BindView(R.id.badgeGb) TextView _countgb;
    @BindView(R.id.badgePlb) TextView _countplb;
    @BindView(R.id.badgeKite) TextView _countkite;
    @BindView(R.id.badgeBkc) TextView _countbkc;
    @BindView(R.id.badgeTps) TextView _counttps;
    @BindView(R.id.pieSum) PieChart sumChart;
    @BindView(R.id.grpAkt) PieChart aktChart;
    @BindView(R.id.grpCnt)
    BarChart cntChart;
    @BindView(R.id.grpFou) BarChart fouChart;
    @BindView(R.id.mainGrid)
    GridLayout mainGrid;

    public FragBcpwkDash() {
        // Required empty public constructor
    }

    public static FragBcpwkDash newInstance() {
        return new FragBcpwkDash();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.frag_bcpwk_dash, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){

        ButterKnife.bind(this, view);

        Locale locale = new Locale("en","EN");
        _session = new SessionManage(getActivity().getApplicationContext());
        HashMap<String, String> user = _session.getUserDetails();
        _token  = user.get(SessionManage.KEY_NAME);
        _numfor = NumberFormat.getNumberInstance(locale);
        _dialog = new ProgressDialog(getActivity());
        _dialog.setMessage("Mohon tunggu...");

        setSingleEvent(mainGrid);

        // total data fasilitas
        sumChart.setUsePercentValues(true);
        sumChart.getDescription().setEnabled(false);
        sumChart.setDrawHoleEnabled(true);
        sumChart.setHoleColor(Color.parseColor("#2E3858"));
        sumChart.setTransparentCircleColor(Color.WHITE);
        sumChart.setTransparentCircleAlpha(110);
        sumChart.setHoleRadius(48f);
        sumChart.setTransparentCircleRadius(50f);
        sumChart.setDrawCenterText(true);
        sumChart.setRotationAngle(0);
        sumChart.setRotationEnabled(true);
        sumChart.setHighlightPerTapEnabled(true);
        sumChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        sumChart.setDrawEntryLabels(false);

        Legend scLeg = sumChart.getLegend();
        scLeg.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        scLeg.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        scLeg.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        scLeg.setDrawInside(false);
        scLeg.setXEntrySpace(10f);
        scLeg.setYEntrySpace(0f);
        scLeg.setYOffset(5f);
        scLeg.setTextColor(Color.WHITE);

        // total data aktif
        aktChart.setUsePercentValues(true);
        aktChart.getDescription().setEnabled(false);
        aktChart.setDrawHoleEnabled(true);
        aktChart.setHoleColor(Color.parseColor("#2E3858"));;
        aktChart.setTransparentCircleColor(Color.WHITE);
        aktChart.setTransparentCircleAlpha(110);
        aktChart.setHoleRadius(48f);
        aktChart.setTransparentCircleRadius(50f);
        aktChart.setDrawCenterText(true);
        aktChart.setRotationAngle(0);
        aktChart.setRotationEnabled(true);
        aktChart.setHighlightPerTapEnabled(true);
        aktChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        aktChart.setDrawEntryLabels(false);

        Legend acLeg = aktChart.getLegend();
        acLeg.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        acLeg.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        acLeg.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        acLeg.setDrawInside(false);
        acLeg.setXEntrySpace(10f);
        acLeg.setYEntrySpace(0f);
        acLeg.setYOffset(5f);
        acLeg.setTextColor(Color.WHITE);

        // grafik negara
        Legend ccLeg = cntChart.getLegend();
        ccLeg.setEnabled(false);

        // grafik foul
        Legend fcLeg = fouChart.getLegend();
        fcLeg.setEnabled(false);

        showDialog();
        jsonData();

    }

    private void jsonData(){

        _apiend = AppVar.IKBC_URL;
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                _apiend,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideDialog();
                        try {
                            Boolean sukses = response.getBoolean("success");
                            String pesan = response.getString("message");

                            if (sukses) {
                                JSONObject data = response.getJSONObject("data");
                                JSONArray total = data.getJSONArray("_total");
                                JSONObject graph = data.getJSONObject("_graph");

                                // total data
                                JSONObject itota = total.getJSONObject(0);

                                int pkb = itota.getInt("pkb");
                                int pgb = itota.getInt("pgb");
                                int plb = itota.getInt("plb");
                                int kit = itota.getInt("kit");
                                int bkc = itota.getInt("bkc");
                                int tps = itota.getInt("tps");

                                _countkb.setText(String.valueOf(pkb));
                                _countgb.setText(String.valueOf(pgb));
                                _countplb.setText(String.valueOf(plb));
                                _countkite.setText(String.valueOf(kit));
                                _countbkc.setText(String.valueOf(bkc));
                                _counttps.setText(String.valueOf(tps));

                                // graph total fasilitas
                                ArrayList<PieEntry> datSum = new ArrayList<>();
                                JSONArray cpfas = graph.getJSONArray("cpfas");
                                for (int i = 0; i < cpfas.length(); i++) {
                                    JSONObject obj = cpfas.getJSONObject(i);
                                    datSum.add(new PieEntry(obj.getInt("total"), obj.getString("fasil")));
                                }
                                PieDataSet dataSet = new PieDataSet(datSum,"" );
                                dataSet.setColors(ColorTemplate.LIBERTY_COLORS);
                                PieData dataSum = new PieData(dataSet);
                                dataSum.setValueFormatter(new PercentFormatter());
                                dataSum.setValueTextSize(8f);
                                dataSum.setValueTextColor(Color.BLACK);
                                sumChart.setData(dataSum);
                                sumChart.invalidate(); // refresh

                                // graph status aktif
                                ArrayList<PieEntry> datAkt = new ArrayList<>();
                                JSONArray aktif = graph.getJSONArray("aktif");
                                for (int i = 0; i < aktif.length(); i++) {
                                    JSONObject akt = aktif.getJSONObject(i);
                                    datAkt.add(new PieEntry(akt.getInt("nilai"), akt.getString("judul")));
                                }

                                PieDataSet dataAkt = new PieDataSet(datAkt,"");
                                dataAkt.setColors(ColorTemplate.LIBERTY_COLORS);
                                PieData dataSts = new PieData(dataAkt);
                                dataSts.setValueFormatter(new PercentFormatter());
                                dataSts.setValueTextSize(8f);
                                dataSts.setValueTextColor(Color.BLACK);
                                aktChart.setData(dataSts);
                                aktChart.invalidate();

                                // graph negara
                                List<BarEntry> datCnt = new ArrayList<>();
                                ArrayList<String> lbl = new ArrayList<>();
                                JSONArray negara = graph.getJSONArray("cnpic");
                                for (int i = 0; i < negara.length(); i++) {
                                    JSONObject cnt = negara.getJSONObject(i);
                                    datCnt.add(new BarEntry(i, cnt.getInt("nilai")));
                                    lbl.add(cnt.getString("judul"));
                                }

                                BarDataSet dataCnt = new BarDataSet(datCnt,"");
                                dataCnt.setColors(ColorTemplate.LIBERTY_COLORS);
                                BarData dataNgr = new BarData(dataCnt);
                                cntChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(lbl));
                                cntChart.getAxisLeft().setTextColor(Color.WHITE);
                                cntChart.getAxisRight().setEnabled(false);
                                cntChart.getXAxis().setDrawGridLines(false);
                                cntChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
                                cntChart.getXAxis().setTextColor(Color.WHITE);
                                cntChart.setFitBars(true);
                                cntChart.setData(dataNgr);
                                cntChart.getBarData().setValueTextColor(Color.WHITE);
                                cntChart.getBarData().setValueTextSize(10f);
                                cntChart.getDescription().setEnabled(false);
                                cntChart.invalidate();

                                // graph foul
                                List<BarEntry> datFou= new ArrayList<>();
                                ArrayList<String> lbel = new ArrayList<>();
                                JSONArray foul = graph.getJSONArray("tfoul");
                                for (int i = 0; i < foul.length(); i++) {
                                    JSONObject fou = foul.getJSONObject(i);
                                    datFou.add(new BarEntry(i, fou.getInt("nilai")));
                                    lbel.add(fou.getString("judul"));
                                }

                                BarDataSet dataFou = new BarDataSet(datFou,"");
                                dataFou.setColors(ColorTemplate.LIBERTY_COLORS);
                                BarData dataPel= new BarData(dataFou);
                                fouChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(lbel));
                                fouChart.getAxisLeft().setTextColor(Color.WHITE);
                                fouChart.getAxisRight().setEnabled(false);
                                fouChart.getXAxis().setDrawGridLines(false);
                                fouChart.getXAxis().setDrawLabels(true);
                                fouChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
                                fouChart.getXAxis().setTextColor(Color.WHITE);
                                fouChart.setFitBars(true);
                                fouChart.getXAxis().setGranularity(1f);
                                fouChart.setData(dataPel);
                                fouChart.getBarData().setValueTextColor(Color.WHITE);
                                fouChart.getBarData().setValueTextSize(10f);
                                fouChart.getDescription().setEnabled(false);
                                fouChart.invalidate();

                            } else {
                                Toast.makeText(getActivity().getApplicationContext(), pesan, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError){
                    Toast.makeText(getActivity().getApplicationContext(), "Connection time out", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(getActivity().getApplicationContext(), "Server problem", Toast.LENGTH_SHORT).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(getActivity().getApplicationContext(), "Internet problem", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(getActivity().getApplicationContext(), "Parsing problem", Toast.LENGTH_SHORT).show();
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() {
                headers = new HashMap<String, String>();
                headers.put("x-access-token", _token);
                headers.put("kantor", "050800");
                return headers;
            }
        };
        requestQueue.add(jsonObjectRequest);

    }

    private void setToggleEvent(GridLayout mainGrid) {
        //Loop all child item of Main Grid
        for (int i = 0; i < mainGrid.getChildCount(); i++) {
            //You can see , all child item is CardView , so we just cast object to CardView
            final CardView cardView = (CardView) mainGrid.getChildAt(i);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (cardView.getCardBackgroundColor().getDefaultColor() == -1) {
                        //Change background color
                        cardView.setCardBackgroundColor(Color.parseColor("#FF6F00"));
                        Toast.makeText(getActivity().getApplicationContext(), "State : True", Toast.LENGTH_SHORT).show();
                    } else {
                        //Change background color
                        cardView.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                        Toast.makeText(getActivity().getApplicationContext(), "State : False", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void setSingleEvent(GridLayout mainGrid) {
        for (int i = 0; i < mainGrid.getChildCount(); i++) {
            CardView cardView = (CardView) mainGrid.getChildAt(i);
            final int finalI = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (finalI) {
                        case 1:
                            intent = new Intent(getActivity().getApplicationContext(), InfoPtgb.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("refkan", "050800");
                            getActivity().getApplicationContext().startActivity(intent);
                            break;
                        case 2:
                            intent = new Intent(getActivity().getApplicationContext(), InfoPplb.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("refkan", "050800");
                            getActivity().getApplicationContext().startActivity(intent);
                            break;
                        case 3:
                            intent = new Intent(getActivity().getApplicationContext(), InfoKite.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("refkan", "050800");
                            getActivity().getApplicationContext().startActivity(intent);
                            break;
                        case 4:
                            intent = new Intent(getActivity().getApplicationContext(), InfoNbkc.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("refkan", "050800");
                            getActivity().getApplicationContext().startActivity(intent);
                            break;
                        case 5:
                            Toast.makeText(getActivity().getApplicationContext(), "Infografis tidak tersedia.", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            intent = new Intent(getActivity().getApplicationContext(), InfoPtkb.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("refkan", "050800");
                            getActivity().getApplicationContext().startActivity(intent);
                            break;
                    }
                }
            });
        }
    }

    private void showDialog() {
        if (!_dialog.isShowing())
            _dialog.show();
    }

    private void hideDialog() {
        if (_dialog.isShowing())
            _dialog.dismiss();
    }

}
