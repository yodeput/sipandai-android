package com.elwilis.emsitpro.trinitymobile;

/**
 * Created by rezar on 31/01/18.
 */

public class SliderUtils {

    String sliderImageUrl;

    public String getSliderImageUrl() {
        return sliderImageUrl;
    }

    public void setSliderImageUrl(String sliderImageUrl) {
        this.sliderImageUrl = sliderImageUrl;
    }
}
