package com.elwilis.emsitpro.trinitymobile.bcpurwakarta;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.elwilis.emsitpro.trinitymobile.AppVar;
import com.elwilis.emsitpro.trinitymobile.R;
import com.elwilis.emsitpro.trinitymobile.SessionManage;
import com.elwilis.emsitpro.trinitymobile.locationmodel.ListLocationModel;
import com.elwilis.emsitpro.trinitymobile.locationmodel.LocationModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PwkMapsFragment extends Fragment implements OnMapReadyCallback, AdapterView.OnItemSelectedListener {

    // general
    Context mContext;
    SupportMapFragment supportMapFragment;
    GoogleMap googleMap;
    MapView mapView;
    View mView;
    List<LocationModel> mListMarker = new ArrayList<>();
    ListLocationModel listLocationModel;
    ProgressDialog pDialog;
    SessionManage session;
    Spinner fasilitas;
    LatLng initLoc;
    BottomNavigationView _btmnav;
    FusedLocationProviderClient mFusedLocationClient;
    protected Location mLastLocation;
    Map<String, String> headers;

    // holder
    String token, apiEnd, lattitude, longitude, userLattLang;
    Double lat, lon, userLatt, userLong;

    public static PwkMapsFragment newInstance() {
        return new PwkMapsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_pwk_maps, container, false);
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Mohon tunggu...");
        showDialog();

        mContext = getActivity().getApplicationContext();

        session = new SessionManage(getActivity().getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        token = user.get(SessionManage.KEY_NAME);

        listLocationModel = new ListLocationModel(mListMarker);

        _btmnav = getActivity().findViewById(R.id.bottom_navigation);

        fasilitas = getActivity().findViewById(R.id.optFas);
        fasilitas.setOnItemSelectedListener(this);
        String item = fasilitas.getItemAtPosition(fasilitas.getSelectedItemPosition()).toString();
        switch (item) {
            case "Kawasan Berikat":
                apiEnd = AppVar.GMAP_KB;
                break;
            case "Gudang Berikat":
                apiEnd = AppVar.GMAP_GB;
                break;
            case "Pusat Logistik Berikat":
                apiEnd = AppVar.GMAP_LB;
                break;
            case "KITE":
                apiEnd = AppVar.GMAP_KT;
                break;
            case "Barang Kena Cukai":
                apiEnd = AppVar.GMAP_KC;
                break;
            case "Kawasan Pabean - TPS":
                apiEnd = AppVar.GMAP_TP;
                break;
        }
        mapView = mView.findViewById(R.id.gmap);
        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id){
        String item = parent.getItemAtPosition(position).toString();
        pDialog.setMessage("Mohon tunggu...");
        showDialog();
        switch (item) {
            case "Kawasan Berikat":
                apiEnd = AppVar.GMAP_KB;
                break;
            case "Gudang Berikat":
                apiEnd = AppVar.GMAP_GB;
                break;
            case "Pusat Logistik Berikat":
                apiEnd = AppVar.GMAP_LB;
                break;
            case "KITE":
                apiEnd = AppVar.GMAP_KT;
                break;
            case "Barang Kena Cukai":
                apiEnd = AppVar.GMAP_KC;
                break;
            case "Kawasan Pabean - TPS":
                apiEnd = AppVar.GMAP_TP;
                break;
        }
        mListMarker.clear();
        mapView = mView.findViewById(R.id.gmap);
        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setPadding(0,0,0,20);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-6.612234,106.8103553), 6));
        loadJsonData();
    }

    private void loadJsonData(){

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                apiEnd,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideDialog();
                        try {
                            Boolean sukses = response.getBoolean("success");
                            String pesan = response.getString("message");
                            if (sukses) {
                                JSONObject data = response.getJSONObject("data");
                                JSONArray datas = data.getJSONArray("_datas");
                                for (int i = 0; i < datas.length(); i++) {
                                    JSONObject obj = datas.getJSONObject(i);
                                    LocationModel locationModel = new LocationModel(
                                            obj.getInt("seqs"),
                                            obj.getString("nama"),
                                            obj.getDouble("latt"),
                                            obj.getDouble("long")
                                    );
                                    mListMarker.add(locationModel);
                                }
                                setMarker(mListMarker);

                                JSONArray total = data.getJSONArray("_total");
                                JSONObject valu = total.getJSONObject(0);

                                _btmnav.getMenu().getItem(0).setTitle("Total: " + valu.getString("total"));
                                _btmnav.getMenu().getItem(1).setTitle("Total: " + valu.getString("tmaps"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError){
                    Toast.makeText(mContext, "Connection time out", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(mContext, "Server problem", Toast.LENGTH_SHORT).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(mContext, "Internet problem", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(mContext, "Parsing problem", Toast.LENGTH_SHORT).show();
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() {
                headers = new HashMap<String, String>();
                headers.put("x-access-token", token);
                headers.put("kantor", "050800");
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    @SuppressWarnings("MissingPermission")
    private void setMarker(List<LocationModel> listData){
        for (int i = 0; i < mListMarker.size(); i++){
            LatLng location = new LatLng(mListMarker.get(i).getLatComp(), mListMarker.get(i).getLonComp());
            googleMap.addMarker(new MarkerOptions().position(location).title(mListMarker.get(i).getNamComp()));
        }
    }

    // show dialog method
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
}
