package com.elwilis.emsitpro.trinitymobile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ForgotPassStep1 extends AppCompatActivity {

    private Context mContext;
    private EditText umail;
    private Button btnFirst;
    private ProgressDialog pDialog;
    private String umailHolder;
    private boolean checkEmpty;
    private String loginEndPoint;
    ConnectivityManager conMgr;
    SessionManage session;
    public static final String EXTRA_EMAIL = "com.elwilis.emsitpro.trinitymobile.email";
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass_step1);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // activity context
        mContext = ForgotPassStep1.this;
        // internet connectivity
        conMgr = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        // session init
        session = new SessionManage(getApplicationContext());
        // view init
        pDialog = new ProgressDialog(mContext);
        umail = findViewById(R.id.umail);
        btnFirst = (Button) findViewById(R.id.btnFor1);
        // click listener
        btnFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // call method
                checkField();
                TextInputLayout til = findViewById(R.id.errval);
                if (checkEmpty) {
                    final String userMail = umail.getText().toString().trim();
                    if (isValidEmail(userMail)){
                        if (conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable() && conMgr.getActiveNetworkInfo().isConnected()){
                            checkEmail();
                        } else {
                            Toast.makeText(mContext, "Tidak ada koneksi internet.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        til.setError("Alamat email tidak valid.");
                    }
                } else {
                    til.setError("Alamat email wajib diisi.");
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void checkEmail(){
        final String userMail = umail.getText().toString().trim();
        pDialog.setMessage("Proses Permintaan...");
        showDialog();
        loginEndPoint = AppVar.EMAIL_URL;
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest strReq = new StringRequest(
                Request.Method.POST,
                loginEndPoint,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideDialog();
                        try {
                            JSONObject rsp = new JSONObject(response);
                            Boolean sukses = rsp.getBoolean("success");
                            String pesan = rsp.getString("message");
                            if (sukses == AppVar.RESPONSE_SUCCESS) {
                                gotoSecond();
                            } else {
                                Toast.makeText(ForgotPassStep1.this, pesan, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){
                        hideDialog();
                        Toast.makeText(ForgotPassStep1.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
        ){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("usrmail",userMail);
                return params;
            }

            @Override
            public Map<String,String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };

        requestQueue.add(strReq);
    }

    // redirect to step 2
    private void gotoSecond() {
        Intent intent = new Intent(mContext, ForgotPassStep2.class);
        final String userMail = umail.getText().toString().trim();
        intent.putExtra(EXTRA_EMAIL, userMail);
        startActivity(intent);
    }

    // check empty
    private void checkField(){
        umailHolder = umail.getText().toString().trim();
        checkEmpty = !TextUtils.isEmpty(umailHolder);
    }

    // email validation
    private static boolean isValidEmail(String umailHolder) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(umailHolder).matches();
    }

    // show dialog method
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    // hide dialog method
    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
