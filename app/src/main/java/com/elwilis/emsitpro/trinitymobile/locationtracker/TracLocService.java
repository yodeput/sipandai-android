package com.elwilis.emsitpro.trinitymobile.locationtracker;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.elwilis.emsitpro.trinitymobile.R;
import com.elwilis.emsitpro.trinitymobile.SessionManage;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TracLocService extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private Location mylocation;
    private GoogleApiClient googleApiClient;
    LocationRequest locationRequest = new LocationRequest();
    private static final String TAG = "Device Tracking";

    public static final String ACTION_LOCATION_BROADCAST = TracLocService.class.getName() + "Location Broadcast";
    public static final String EXTRA_LATITUDE = "extra_latitude";
    public static final String EXTRA_LONGITUDE = "extra_longitude";
    private boolean startedService = false;

    SessionManage session;
    String apiEnd, usrnip;

    public TracLocService() {}

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        googleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        locationRequest.setInterval(1800000);
        locationRequest.setFastestInterval(900000);
        int priority = LocationRequest.PRIORITY_HIGH_ACCURACY;
        locationRequest.setPriority(priority);
        googleApiClient.connect();
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        session = new SessionManage(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        usrnip = user.get(SessionManage.KEY_NIP);
    }

    @Override
    public void onDestroy() {
        startedService = false;
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        // throw new UnsupportedOperationException("Not yet implemented");
        return null;
    }

    private float getBatteryLevel() {
        Intent batteryStatus = registerReceiver(null, new android.content.IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int batteryLevel = -1;
        int batteryScale = 1;
        if (batteryStatus != null) {
            batteryLevel = batteryStatus.getIntExtra(android.os.BatteryManager.EXTRA_LEVEL, batteryLevel);
            batteryScale = batteryStatus.getIntExtra(android.os.BatteryManager.EXTRA_SCALE, batteryScale);
        }
        return batteryLevel / (float) batteryScale * 100;
    }

    @Override
    public void onLocationChanged(Location location) {
        mylocation = location;
        if (mylocation != null) {
            Double latitude  = mylocation.getLatitude();
            Double longitude = mylocation.getLongitude();
            Timestamp timestamp = new Timestamp(new Date().getTime());
            trackDevice(latitude, longitude, timestamp);
            sendMessageToUI(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
        }
    }

    private void trackDevice(final Double latitude, final Double longitude, final Timestamp timestamp) {
        apiEnd = com.elwilis.emsitpro.trinitymobile.AppVar.LOGS_TRC;
        com.android.volley.RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(
                Request.Method.PUT,
                apiEnd,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {}
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError || error instanceof NoConnectionError){
                            // Toast.makeText(getApplicationContext(), "Connection time out", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ServerError) {
                            // Toast.makeText(getApplicationContext(), "Server problem", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof NetworkError) {
                            // Toast.makeText(getApplicationContext(), "Internet problem", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ParseError) {
                            // Toast.makeText(getApplicationContext(), "Parsing problem", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        ){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("usernip", usrnip);
                params.put("userbat", String.valueOf(getBatteryLevel()));
                params.put("userlat", String.valueOf(latitude));
                params.put("userlon", String.valueOf(longitude));
                params.put("usertim", String.valueOf(new Date().getTime()));
                params.put("usersqt", String.valueOf(timestamp));
                return params;
            }
            @Override
            public Map<String,String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    // Location callbacks
    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Log.d(TAG, "== Error On onConnected() Permission not granted");
            //Permission not granted by user so cancel the further execution.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        Log.d(TAG, "Connected to Google API");
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Do whatever you need
        //You can display a message here
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //You can display a message here
    }

    private void sendMessageToUI(String lat, String lng) {

        Log.d(TAG, "Sending info...");
        Intent intent = new Intent(ACTION_LOCATION_BROADCAST);
        intent.putExtra(EXTRA_LATITUDE, lat);
        intent.putExtra(EXTRA_LONGITUDE, lng);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

    }

}
