package com.elwilis.emsitpro.trinitymobile.livereport;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.elwilis.emsitpro.trinitymobile.AppVar;
import com.elwilis.emsitpro.trinitymobile.R;
import com.elwilis.emsitpro.trinitymobile.SessionManage;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NoteActivity extends AppCompatActivity implements OnMapReadyCallback {

    // view
    @BindView(R.id.toolbar) Toolbar _toolbar;
    @BindView(R.id.kantKpbc) TextView _kantKpbc;
    @BindView(R.id.namaComp) TextView _namaComp;
    @BindView(R.id.namaUser) TextView _namaUser;
    @BindView(R.id.timeLaps) TextView _timeLaps;
    @BindView(R.id.subjRepo) TextView _subjRepo;
    @BindView(R.id.contRepo) TextView _contRepo;
    @BindView(R.id.msgImage) ViewPager _msgImage;
    @BindView(R.id.SliderDots) LinearLayout _slidDots;

    // general
    Context context;
    ProgressDialog pDialog;
    SessionManage session;
    NumberFormat numberFormat;
    Locale locale;
    Intent intent;
    Bundle bundle;
    ViewPager viewPager;
    PageAdapter pageAdapter;
    int dotscount;
    ImageView[] dots;
    GoogleMap googleMap;
    MapView mapView;

    // holder
    private static final int NOTIFICATION_ID = 1;
    String tokEns, seqRpt, usrNip, apiEnd, refSes;
    Double lokLat, lokLon, lokAll;
    List<SlideUtils> sliderImg;
    private Map<String, String> headers;
    RequestQueue rq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        ButterKnife.bind(this);
        setSupportActionBar(_toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);

        session = new SessionManage(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        tokEns = user.get(SessionManage.KEY_NAME);
        usrNip = user.get(SessionManage.KEY_NIP);

        Intent intent = getIntent();
        seqRpt = intent.getStringExtra("seqs_note");
        refSes = intent.getStringExtra("refs_sess");

        rq = VolleyRequest.getInstance(this).getRequestQueue();
        pDialog = new ProgressDialog(this);
        sliderImg = new ArrayList<>();

        pDialog.setMessage("Mohon tunggu...");
        showDialog();

        sendRequest();

        _msgImage.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for(int i = 0; i< dotscount; i++){
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.nonactive_dot));
                }
                dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mapView = (MapView) findViewById(R.id.lapsMaps);
        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
    }

    private void sendRequest(){
        apiEnd = AppVar.DETS_RP;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                apiEnd,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideDialog();
                        try {
                            Boolean sukses = response.getBoolean("success");
                            String pesan = response.getString("message");
                            if (sukses == AppVar.RESPONSE_SUCCESS) {
                                JSONObject data = response.getJSONObject("data");

                                JSONObject list = data.getJSONObject("_list");
                                _kantKpbc.setText(String.format("%s / %s", cekNull(list.getString("kat_kref")), list.getString("ref_fasi")));
                                _namaComp.setText(list.getString("ref_comp"));
                                _namaUser.setText(list.getString("nam_user"));
                                _timeLaps.setText(tglHelper(list.getString("cre_date")));
                                _subjRepo.setText(list.getString("jud_note"));
                                _contRepo.setText(list.getString("isi_note"));

                                String location = list.getString("ref_kord");
                                if (!cekNull(location).equals("-")) {
                                    String [] lokval = location.split(",");
                                    lokLat = Double.valueOf(lokval[0]);
                                    lokLon = Double.valueOf(lokval[1]);
                                } else {
                                    lokLat = 0.0;
                                    lokLon = 0.0;
                                }

                                // get detail data
                                JSONArray foto = data.getJSONArray("_foto");
                                if (foto.length() > 0) {
                                    for (int i = 0; i < foto.length(); i++) {
                                        SlideUtils sliderUtils = new SlideUtils();
                                        try {
                                            JSONObject jsonObject = foto.getJSONObject(i);
                                            sliderUtils.setSlideImageUrl("http://sipandaibcjabar.com/statics/repo/note/"+ jsonObject.getString("img_name"));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        sliderImg.add(sliderUtils);
                                    }
                                } else {
                                    SlideUtils sliderUtils = new SlideUtils();
                                    sliderUtils.setSlideImageUrl("http://sipandaibcjabar.com/statics/img/empty_image.jpeg");
                                    sliderImg.add(sliderUtils);
                                }

                                pageAdapter = new PageAdapter(sliderImg, getApplicationContext());
                                _msgImage.setAdapter(pageAdapter);

                                dotscount = pageAdapter.getCount();
                                dots = new ImageView[dotscount];

                                for(int i = 0; i < dotscount; i++){
                                    dots[i] = new ImageView(getApplicationContext());
                                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.nonactive_dot));
                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                    params.setMargins(8, 0, 8, 0);
                                    _slidDots.addView(dots[i], params);
                                }
                                dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));

                            } else {
                                Toast.makeText(context, pesan, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideDialog();
                        if (error instanceof TimeoutError || error instanceof NoConnectionError){
                             Toast.makeText(context, "Connection time out", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ServerError) {
                             Toast.makeText(context, "Server problem", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof NetworkError) {
                             Toast.makeText(context, "Internet problem", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ParseError) {
                             Toast.makeText(context, "Parsing problem", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() {
                headers = new HashMap<String, String>();
                headers.put("x-access-token", tokEns);
                headers.put("sequence", seqRpt);
                headers.put("refssess", refSes);
                headers.put("nipusers", usrNip);
                return headers;
            }
        };
        VolleyRequest.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        final LatLng location;
        this.googleMap = googleMap;
        if (lokLat != 0.0 && lokLon != 0.0) {
            location = new LatLng(lokLat, lokLon);
            googleMap.addMarker(new MarkerOptions().position(location).title("LOKASI PELAPORAN"));
        } else {
            location = new LatLng(-6.612234,106.8103553);
        }
        googleMap.setPadding(0,0,0,20);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 8));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    // show dialog method
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    // hide dialog method
    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private String tglHelper(String params){
        String[] sourceParams = params.split("-");
        String tahun = sourceParams[0];
        String bulan = sourceParams[1];
        String tangg = sourceParams[2].substring(0,2);

        String[] WaktuParams = params.split(" ");
        String waktu = WaktuParams[0].substring(11);
        String[] detWak = waktu.split(":");
        String jam = detWak[0];
        String menit = detWak[1];
        String detik = detWak[2].substring(0,2);

        return tangg + "/" + bulan + "/" + tahun + " " + jam + ":" + menit + ":" + detik + " WIB";
    }

    private String cekNull (String params) {
        if (params != null && !params.isEmpty() && params != "null") {
            return params;
        } else {
            return "-";
        }
    }
}
