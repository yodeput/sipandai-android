package com.elwilis.emsitpro.trinitymobile.detail;

public class FoulObject {

    private String refFoul, kpbFoul, nomFoul, tglFoul, stsFoul, comFoul;

    public FoulObject(String refFoul, String kpbFoul, String nomFoul, String tglFoul, String stsFoul, String comFoul)
    {
        this.refFoul = refFoul;
        this.kpbFoul = kpbFoul;
        this.nomFoul = nomFoul;
        this.tglFoul = tglFoul;
        this.stsFoul = stsFoul;
        this.comFoul = comFoul;
    }

    public String getRefFoul() {return  refFoul;}
    public String getKpbFoul() {return  kpbFoul;}
    public String getNomFoul() {return  nomFoul;}
    public String getTglFoul() {return  tglFoul;}
    public String getStsFoul() {return  stsFoul;}
    public String getComFoul() {return  comFoul;}
}
