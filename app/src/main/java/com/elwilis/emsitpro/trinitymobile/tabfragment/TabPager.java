package com.elwilis.emsitpro.trinitymobile.tabfragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by rezar on 06/02/18.
 */

public class TabPager extends FragmentStatePagerAdapter {

    int tabCount;

    public TabPager(FragmentManager fm, int tabCount){
        super(fm);
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position){
        switch (position)
        {
            case 0:
                InfoGrafis tab1 = new InfoGrafis();
                return tab1;
            case 1:
                ListPesan tab2 = new ListPesan();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }

}
