package com.elwilis.emsitpro.trinitymobile;

import android.view.View;

/**
 * Created by rezar on 29/01/18.
 */

public interface ItemClickListener {

    void onItemClick(View rootView, int i);
}
