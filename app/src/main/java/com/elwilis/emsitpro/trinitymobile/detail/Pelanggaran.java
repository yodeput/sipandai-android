package com.elwilis.emsitpro.trinitymobile.detail;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.elwilis.emsitpro.trinitymobile.AppVar;
import com.elwilis.emsitpro.trinitymobile.R;
import com.elwilis.emsitpro.trinitymobile.SessionManage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Pelanggaran extends AppCompatActivity {

    // general
    Context context;
    Intent intent;
    RecyclerView recyclerView;
    ProgressDialog pDialog;
    SessionManage session;
    List<FoulObject> foulList;
    FoulAdapter foulAdapter;
    Map<String, String> headers;

    // holder
    String apiEnd, token, item, items, refFol, namCom, namKan, fasCom;

    @BindView(R.id.foul_toolbar) Toolbar _toolbar;
    @BindView(R.id.namaKpbc) TextView _namaKpbc;
    @BindView(R.id.namaComp) TextView _namaComp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foul);
        ButterKnife.bind(this);
        setSupportActionBar(_toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        context = getApplicationContext();

        session = new SessionManage(context);
        HashMap<String, String> user = session.getUserDetails();
        token = user.get(SessionManage.KEY_NAME);

        intent = getIntent();
        refFol = intent.getStringExtra("refs_foul");
        namCom = intent.getStringExtra("refs_comp");
        namKan = intent.getStringExtra("refs_kpbc");
        fasCom = intent.getStringExtra("refs_fasi");

        _namaComp.setText(namCom);
        _namaKpbc.setText(namKan);

        recyclerView = findViewById(R.id.foulList);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        foulList = new ArrayList<>();
        foulAdapter = new FoulAdapter(context, foulList, getSupportFragmentManager());
        recyclerView.setAdapter(foulAdapter);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Mohon tunggu...");
        showDialog();

        apiEnd = AppVar.FOUL_FS;
        loadJSON();
    }

    private void loadJSON(){
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                apiEnd,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideDialog();
                        try {
                            Boolean sukses = response.getBoolean("success");
                            String pesan = response.getString("message");
                            if (sukses) {
                                JSONObject data = response.getJSONObject("data");
                                JSONArray jsonArray = data.getJSONArray("_result");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    FoulObject fbj = new FoulObject(
                                            jsonObject.getString("ref_foul"),
                                            jsonObject.getString("kantor"),
                                            jsonObject.getString("nomsbp"),
                                            tglHelper(jsonObject.getString("tglsbp")),
                                            jsonObject.getString("kodpnd"),
                                            namCom
                                    );
                                    foulList.add(fbj);
                                }
                                foulAdapter.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError){
                    Toast.makeText(context, "Connection time out", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(context, "Server problem", Toast.LENGTH_SHORT).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(context, "Internet problem", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(context, "Parsing problem", Toast.LENGTH_SHORT).show();
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() {
                headers = new HashMap<String, String>();
                headers.put("x-access-token", token);
                headers.put("x-params-datafoul", "018244079055000");
                headers.put("x-params-datafasi", fasCom);
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private String tglHelper(String params){
        String[] sourceParams = params.split("-");
        String tahun = sourceParams[0];
        String bulan = sourceParams[1];
        String tangg = sourceParams[2].substring(0,2);

        return tangg + "/" + bulan + "/" + tahun;
    }
}
