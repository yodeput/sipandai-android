package com.elwilis.emsitpro.trinitymobile;

/**
 * Created by rezar on 22/01/18.
 */

public class AppVar {
    //URL to our REST API
    public static final String API_URL = "http://api.sipandaibcjabar.com/api/v3/";
    // REST API Endpoint --> User Login & Forgot Password
    public static final String LOGIN_URL = API_URL + "authentication/app";
    public static final String EMAIL_URL = API_URL + "forget/email/check";
    public static final String RESP_URL = API_URL + "forget/email/ucode";
    public static final String NEWP_URL = API_URL + "user/update/pass";
    public static final String USER_FBT = API_URL + "user/tracking/fbt";
    // REST API Endpoint --> Infografis
    public static final String IKWB_URL = API_URL + "infografis/dashkwl";
    public static final String IKBC_URL = API_URL + "infografis/dashkbc";
    public static final String KWKB_URL = API_URL + "infografis/dkwlpkb";
    public static final String KWGB_URL = API_URL + "infografis/dkwlpgb";
    public static final String KWLB_URL = API_URL + "infografis/dkwlplb";
    public static final String KWKT_URL = API_URL + "infografis/dkwlkit";
    public static final String KWKC_URL = API_URL + "infografis/dkwlbkc";
    public static final String KWSK_URL = API_URL + "infografis/dkwlskp";
    public static final String BCKB_URL = API_URL + "infografis/dkbcpkb";
    public static final String BCGB_URL = API_URL + "infografis/dkbcpgb";
    public static final String BCLB_URL = API_URL + "infografis/dkbcplb";
    public static final String BCKT_URL = API_URL + "infografis/dkbckit";
    public static final String BCKC_URL = API_URL + "infografis/dkbcbkc";
    public static final String BCSK_URL = API_URL + "infografis/dkbcskp";

    // REST API Endpoint --> Fasilitas
    // --> KB
    public static final String DASH_KB = API_URL + "company/dashckb";
    public static final String GMAP_KB = API_URL + "company/mapsckb";
    public static final String GETS_KB = API_URL + "company/compckb";
    public static final String UGPS_KB = API_URL + "company/updackb";
    // --> GB
    public static final String DASH_GB = API_URL + "company/dashcgb";
    public static final String GMAP_GB = API_URL + "company/mapscgb";
    public static final String GETS_GB = API_URL + "company/compcgb";
    public static final String UGPS_GB = API_URL + "company/updacgb";
    // --> PLB
    public static final String DASH_LB = API_URL + "company/dashclb";
    public static final String GMAP_LB = API_URL + "company/mapsclb";
    public static final String GETS_LB = API_URL + "company/compclb";
    public static final String UGPS_LB = API_URL + "company/updaclb";
    // --> KITE
    public static final String DASH_KT = API_URL + "company/dashckt";
    public static final String GMAP_KT = API_URL + "company/mapsckt";
    public static final String GETS_KT = API_URL + "company/compckt";
    public static final String UGPS_KT = API_URL + "company/updackt";
    // --> BKC
    public static final String DASH_KC = API_URL + "company/dashckc";
    public static final String GMAP_KC = API_URL + "company/mapsckc";
    public static final String GETS_KC = API_URL + "company/compckc";
    public static final String UGPS_KC = API_URL + "company/updackc";
    // --> TPS
    public static final String DASH_TP = API_URL + "company/dashctp";
    public static final String GMAP_TP = API_URL + "company/mapsctp";
    public static final String GETS_TP = API_URL + "company/compctp";
    public static final String UGPS_TP = API_URL + "company/updactp";

    // REST API Endpoint --> Live report
    public static final String LIVE_RP = API_URL + "report/listrpt";
    public static final String DETS_RP = API_URL + "report/detsrpt";

    // REST API Endpoint --> Data Pelanggaran
    public static final String FOUL_FS = API_URL + "pelanggaran/fasil";
    public static final String FOUL_DT = API_URL + "pelanggaran/detail";

    //Keys for email and password
    public static final String KEY_NIP = "nip";
    public static final String KEY_PASSWORD = "password";

    //If server response is equal to this that means login is successful
    public static final Boolean RESPONSE_SUCCESS = true;
    public static final Boolean RESPONSE_FAILURE = false;
    public static final String LOGS_TRC = API_URL + "user/tracking/logs";

}
