package com.elwilis.emsitpro.trinitymobile.bccikarang;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.elwilis.emsitpro.trinitymobile.AppVar;
import com.elwilis.emsitpro.trinitymobile.DataAdapter;
import com.elwilis.emsitpro.trinitymobile.DataObject;
import com.elwilis.emsitpro.trinitymobile.R;
import com.elwilis.emsitpro.trinitymobile.SessionManage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;

public class CkrListFragment extends Fragment
        implements SearchView.OnQueryTextListener, AdapterView.OnItemSelectedListener {

    // general
    Context context;
    RecyclerView recyclerView;
    List<DataObject> comFas;
    DataAdapter adapter;
    ProgressDialog pDialog;
    SessionManage session;
    SearchView searchView;
    Spinner fasilitas;
    Map<String, String> headers;
    BottomNavigationView _btmnav;

    // holder
    String apiEnd, token, item, items;

    public static CkrListFragment newInstance() {
        return new CkrListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ckr_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        ButterKnife.bind(getActivity().getApplicationContext(), view);

        session = new SessionManage(getActivity().getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        token = user.get(SessionManage.KEY_NAME);

        context = getActivity().getApplicationContext();
        pDialog = new ProgressDialog(getActivity());

        searchView = getActivity().findViewById(R.id.srcNam);
        searchView.setOnQueryTextListener(this);
        searchView.setIconifiedByDefault(true);

        recyclerView = getActivity().findViewById(R.id.bgrList);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        comFas  = new ArrayList<>();
        adapter = new DataAdapter(getActivity().getApplicationContext(), comFas);
        recyclerView.setAdapter(adapter);

        pDialog.setMessage("Mohon tunggu...");
        showDialog();

        fasilitas = getActivity().findViewById(R.id.optFas);
        fasilitas.setOnItemSelectedListener(this);
        item = fasilitas.getItemAtPosition(fasilitas.getSelectedItemPosition()).toString();

        switch (item) {
            case "Kawasan Berikat":
                apiEnd = AppVar.DASH_KB;
                break;
            case "Gudang Berikat":
                apiEnd = AppVar.DASH_GB;
                break;
            case "Pusat Logistik Berikat":
                apiEnd = AppVar.DASH_LB;
                break;
            case "KITE":
                apiEnd = AppVar.DASH_KT;
                break;
            case "Barang Kena Cukai":
                apiEnd = AppVar.DASH_KC;
                break;
            case "Kawasan Pabean - TPS":
                apiEnd = AppVar.DASH_TP;
                break;
        }

        _btmnav = getActivity().findViewById(R.id.bottom_navigation);
        loadJSON();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id){
        items = parent.getItemAtPosition(position).toString();
        pDialog.setMessage("Mohon tunggu...");
        showDialog();
        switch (items){
            case "Kawasan Berikat":
                apiEnd = AppVar.DASH_KB;
                break;
            case "Gudang Berikat":
                apiEnd = AppVar.DASH_GB;
                break;
            case "Pusat Logistik Berikat":
                apiEnd = AppVar.DASH_LB;
                break;
            case "KITE":
                apiEnd = AppVar.DASH_KT;
                break;
            case "Barang Kena Cukai":
                apiEnd = AppVar.DASH_KC;
                break;
            case "Kawasan Pabean - TPS":
                apiEnd = AppVar.DASH_TP;
                break;
        }
        comFas.clear();
        loadJSON();
    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    private void loadJSON(){
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                apiEnd,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideDialog();
                        try {
                            Boolean sukses = response.getBoolean("success");
                            String pesan = response.getString("message");
                            if (sukses) {
                                JSONObject data = response.getJSONObject("data");
                                JSONArray datas = data.getJSONArray("_datas");
                                if (datas.length() > 0) {
                                    for (int i = 0; i < datas.length(); i++) {
                                        JSONObject obj = datas.getJSONObject(i);
                                        DataObject kbs = new DataObject(
                                                obj.getString("kant"),
                                                obj.getString("nama"),
                                                obj.getString("alam"),
                                                obj.getString("risk"),
                                                obj.getString("date"),
                                                obj.getString("gpsk"),
                                                obj.getString("poin")
                                        );
                                        comFas.add(kbs);
                                    }
                                    adapter.notifyDataSetChanged();
                                } else {
                                    DataObject kbs = new DataObject("", "Data tidak tersedia", "", "", "", "", "");
                                    comFas.add(kbs);
                                    adapter.notifyDataSetChanged();
                                    Toast.makeText(getActivity().getApplicationContext(), "Data tidak tersedia", Toast.LENGTH_SHORT).show();
                                }

                                JSONArray total = data.getJSONArray("_total");
                                JSONObject valu = total.getJSONObject(0);

                                _btmnav.getMenu().getItem(0).setTitle("Total: " + valu.getString("total"));
                                _btmnav.getMenu().getItem(1).setTitle("Total: " + valu.getString("tmaps"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError){
                    Toast.makeText(context, "Connection time out", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(context, "Server problem", Toast.LENGTH_SHORT).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(context, "Internet problem", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(context, "Parsing problem", Toast.LENGTH_SHORT).show();
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() {
                headers = new HashMap<String, String>();
                headers.put("x-access-token", token);
                headers.put("kantor", "051000");
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public boolean onQueryTextChange(String query) {
        adapter.getFilter().filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        adapter.getFilter().filter(query);
        return false;
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
