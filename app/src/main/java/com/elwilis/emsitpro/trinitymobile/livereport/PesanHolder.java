package com.elwilis.emsitpro.trinitymobile.livereport;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.elwilis.emsitpro.trinitymobile.ItemClickListener;
import com.elwilis.emsitpro.trinitymobile.R;

public class PesanHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView subj_laps, kant_kpbc, nama_comp, nama_user, time_laps, seqs_note, refs_sess, refs_read;
    public ItemClickListener itemClickListener;

    public PesanHolder(View itemView) {
        super(itemView);
        this.subj_laps = itemView.findViewById(R.id.subjLaps);
        this.kant_kpbc = itemView.findViewById(R.id.kantKpbc);
        this.nama_comp = itemView.findViewById(R.id.namaComp);
        this.nama_user = itemView.findViewById(R.id.namaUser);
        this.time_laps = itemView.findViewById(R.id.timeLaps);
        this.seqs_note = itemView.findViewById(R.id.seqsNote);
        this.refs_sess = itemView.findViewById(R.id.refsSess);
        this.refs_read = itemView.findViewById(R.id.refsRead);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View rootView){
        this.itemClickListener.onItemClick(rootView,getLayoutPosition());
    }

    public void setItemClickListener(ItemClickListener ic){
        this.itemClickListener = ic;
    }
}
