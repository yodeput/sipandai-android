package com.elwilis.emsitpro.trinitymobile.locationmodel;

import java.util.List;

/**
 * Created by rezar on 02/02/18.
 */

public class ListLocationModel {

    private List<LocationModel> mData;

    public ListLocationModel(List<LocationModel> mData) {
        this.mData = mData;
    }

    public ListLocationModel() {

    }

    public List<LocationModel> getmData() {
        return mData;
    }

    public void setmData(List<LocationModel> mData) {
        this.mData = mData;
    }
}
