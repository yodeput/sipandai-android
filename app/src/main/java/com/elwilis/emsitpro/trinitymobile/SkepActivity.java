package com.elwilis.emsitpro.trinitymobile;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.elwilis.emsitpro.trinitymobile.skepmodel.Child;
import com.elwilis.emsitpro.trinitymobile.skepmodel.ExpListAdapter;
import com.elwilis.emsitpro.trinitymobile.skepmodel.HeadModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SkepActivity extends AppCompatActivity {

    // view
    @BindView(R.id.toolbar) Toolbar _toolbar;
    @BindView(R.id.kbTitle) TextView _kbtitle;
    @BindView(R.id.kbLast) TextView _kblast;

    Context context;
    SessionManage session;
    ProgressDialog pDialog;
    NumberFormat numFor;
    Locale locale;
    Bundle bundle;

    String apiEnd, token, extHol, skpHol, judHol, jskHol;
    Map<String, String> headers;
    ExpListAdapter expListAdapter;
    ExpandableListView expandableListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skep);
        ButterKnife.bind(this);
        setSupportActionBar(_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        context = SkepActivity.this;
        locale  = new Locale("en","EN");
        session = new SessionManage(context);

        HashMap<String, String> user = session.getUserDetails();
        token   = user.get(SessionManage.KEY_NAME);

        numFor  = NumberFormat.getNumberInstance(locale);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Mohon tunggu...");
        showDialog();

        if (savedInstanceState == null) {
            bundle = getIntent().getExtras();
            if (bundle == null) {
                extHol = "";
                skpHol = "";
                judHol = "";
                jskHol = "";
            } else {
                extHol = bundle.getString("refkan");
                skpHol = bundle.getString("refskp");
                judHol = bundle.getString("katskp");
                jskHol = bundle.getString("jenskp");
            }
        }

        switch (jskHol) {
            case "v_skgb":
                getSupportActionBar().setTitle(R.string.info_skp_gb);
                break;
            case "v_sklb":
                getSupportActionBar().setTitle(R.string.info_skp_plb);
                break;
            case "v_skkc":
                getSupportActionBar().setTitle(R.string.info_skp_bkc);
                break;
                default:
                    getSupportActionBar().setTitle(R.string.info_skp_kb);
                    break;
        }

        expandableListView = (ExpandableListView) findViewById(R.id.skepData);
        jsonData(extHol, skpHol, judHol, jskHol);

    }

    private void jsonData(final String extHol, final String skpHol, final String judHol, final String jskHol) {
        if (!"050000".equals(extHol)) {
            apiEnd =  AppVar.BCSK_URL;
        } else {
            apiEnd = AppVar.KWSK_URL;
        }
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                apiEnd,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideDialog();
                        try {
                            Boolean sukses = response.getBoolean("success");
                            String pesan = response.getString("message");
                            if (sukses) {

                                ArrayList<HeadModel> list = new ArrayList<HeadModel>();
                                ArrayList<Child> ch_list;
                                JSONArray data = response.getJSONArray("data");

                                for (int i = 0; i < data.length(); i++) {
                                    HeadModel headModel = new HeadModel();
                                    JSONObject val = data.getJSONObject(i);
                                    ch_list = new ArrayList<Child>();
                                    JSONArray sdata = val.getJSONArray("nilai");
                                    headModel.setName(val.getString("kanto") + " (" + String.valueOf(sdata.length()) + ")" );
                                    for (int j = 0; j < sdata.length(); j++) {
                                        JSONObject sdt = sdata.getJSONObject(j);
                                        Child child = new Child();
                                        child.setName(sdt.getString("nam_comp"));
                                        child.setCaption("Jatuh tempo pada: " + tglHelper(sdt.getString("tgl_skep")));
                                        ch_list.add(child);
                                    }

                                    headModel.setItems(ch_list);
                                    list.add(headModel);
                                }
                                expListAdapter = new ExpListAdapter(context, list);
                                expandableListView.setAdapter(expListAdapter);

                                _kbtitle.setText(judHol);
                                _kblast.setText(kntHelper(extHol));

                            } else {
                                Toast.makeText(context, pesan, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError){
                    Toast.makeText(context, "Connection time out", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(context, "Server problem", Toast.LENGTH_SHORT).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(context, "Internet problem", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(context, "Parsing problem", Toast.LENGTH_SHORT).show();
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() {
                headers = new HashMap<String, String>();
                headers.put("x-access-token", token);
                headers.put("kantor", extHol);
                headers.put("skepval", skpHol);
                headers.put("jenskep", jskHol);
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private String kntHelper(String params) {
        String caption;
        switch (params) {
            case "050300":
                caption = "KPPBC Tipe Madya Pabean A Bogor";
                break;
            case "050500":
                caption = "KPPBC Tipe Madya Pabean A Bandung";
                break;
            case "050600":
                caption = "KPPBC Tipe Pratama Tasikmalaya";
                break;
            case "050700":
                caption = "KPPBC Tipe Madya Pabean C Cirebon";
                break;
            case "050800":
                caption = "KPPBC Tipe Madya Pabean A Purwakarta";
                break;
            case "050900":
                caption = "KPPBC Tipe Madya Pabean A Bekasi";
                break;
            case "051000":
                caption = "KPPBC Tipe Madya Pabean Cikarang";
                break;
            default:
                caption = "Kanwil DJBC Jawa Barat";
        }
        return caption;
    }

    private String tglHelper(String params){
        if (params != null && !params.isEmpty() && !params.equals("null"))
        {
            String[] sourceParams = params.split("-");
            String tahun = sourceParams[0];
            String bulan = sourceParams[1];
            String tangg = sourceParams[2].substring(0,2);
            return tangg + "/" + bulan + "/" + tahun;
        } else {
            return "-";
        }
    }
}
