package com.elwilis.emsitpro.trinitymobile.detail;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.elwilis.emsitpro.trinitymobile.ItemClickListener;
import com.elwilis.emsitpro.trinitymobile.R;

public class FoulHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView refFoul, kpbFoul, nomFoul, stsFoul, comFoul;
    ItemClickListener itemClickListener;
    Context context;

    public FoulHolder(View itemView) {
        super (itemView);
        this.refFoul = itemView.findViewById(R.id.foul_refs);
        this.kpbFoul = itemView.findViewById(R.id.foul_kpbc);
        this.nomFoul = itemView.findViewById(R.id.foul_nsbp);
        this.stsFoul = itemView.findViewById(R.id.foul_stat);
        this.comFoul = itemView.findViewById(R.id.foul_comp);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View rootView){
        this.itemClickListener.onItemClick(rootView,getLayoutPosition());
    }

    public void setItemClickListener(ItemClickListener ic){
        this.itemClickListener = ic;
    }
}
