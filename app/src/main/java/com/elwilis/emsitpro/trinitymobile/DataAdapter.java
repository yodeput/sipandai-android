package com.elwilis.emsitpro.trinitymobile;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.elwilis.emsitpro.trinitymobile.company.GeBe;
import com.elwilis.emsitpro.trinitymobile.company.KaBe;
import com.elwilis.emsitpro.trinitymobile.company.KiTe;
import com.elwilis.emsitpro.trinitymobile.company.NbKc;
import com.elwilis.emsitpro.trinitymobile.company.PeLb;
import com.elwilis.emsitpro.trinitymobile.company.PtPs;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by rezar on 27/01/18.
 */

public class DataAdapter extends RecyclerView.Adapter<FasHolder> implements Filterable {

    private List<DataObject> comFas;
    private List<DataObject> filterlist;
    private Context context;
    private LayoutInflater inflater;
    Intent intent;

    public DataAdapter(Context context, List<DataObject> comFas) {
        this.context = context;
        this.comFas = comFas;
        this.filterlist = comFas;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public FasHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = inflater.inflate(R.layout.card_row, parent, false);
        FasHolder holder = new FasHolder(rootView);
        return holder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(FasHolder holder, final int i) {
        final DataObject dataObject = filterlist.get(i);
        // card_row holder
        holder.namComp.setText(dataObject.getNamComp());
        if (Objects.equals(dataObject.getRefRisk().toLowerCase(), "merah")) {
            holder.namComp.setTextColor(Color.parseColor("#F44336"));
        } else if(Objects.equals(dataObject.getRefRisk().toLowerCase(), "kuning")) {
            holder.namComp.setTextColor(Color.parseColor("#FFC107"));
        } else if(Objects.equals(dataObject.getRefRisk().toLowerCase(), "hijau")) {
            holder.namComp.setTextColor(Color.parseColor("#4caf50"));
        } else {
            holder.namComp.setTextColor(Color.parseColor("#8e9ac4"));
        }
        holder.alaComp.setText(dataObject.getAlaComp());
        if (Objects.equals(dataObject.getRefRisk(), "null")) {
            holder.refRisk.setText("-");
        } else if (Objects.equals(dataObject.getRefRisk(), "")) {
            holder.refRisk.setText("-");
        } else {
            holder.refRisk.setText(dataObject.getRefRisk());
        }
        holder.updDate.setText(tglHelper(dataObject.getUpdDate()));
        if (TextUtils.isEmpty(dataObject.getGpsLoca()) || Objects.equals(dataObject.getGpsLoca(), "null")) {
            holder.gpsLoca.setText("Tidak Tersedia");
        } else {
            holder.gpsLoca.setText("Tersedia");
        }
        holder.refPoin.setText(dataObject.getRefPoin());
        holder.kodKant.setText(dataObject.getKodKant());

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View rootView, int i){
                if (filterlist.get(i).getRefPoin().contains("KB")) {
                    intent = new Intent(context, KaBe.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("refs", filterlist.get(i).getRefPoin());
                    context.startActivity(intent);
                } else if (filterlist.get(i).getRefPoin().contains("GB")) {
                    intent = new Intent(context, GeBe.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("refs", filterlist.get(i).getRefPoin());
                    context.startActivity(intent);
                } else if (filterlist.get(i).getRefPoin().contains("KITE")) {
                    intent = new Intent(context, KiTe.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("refs", filterlist.get(i).getRefPoin());
                    context.startActivity(intent);
                } else if (filterlist.get(i).getRefPoin().contains("BKC")) {
                    intent = new Intent(context, NbKc.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("refs", filterlist.get(i).getRefPoin());
                    context.startActivity(intent);
                } else if (filterlist.get(i).getRefPoin().contains("PLB")) {
                    intent = new Intent(context, PeLb.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("refs", filterlist.get(i).getRefPoin());
                    context.startActivity(intent);
                } else if (filterlist.get(i).getRefPoin().contains("TPS")) {
                    intent = new Intent(context, PtPs.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("refs", filterlist.get(i).getRefPoin());
                    context.startActivity(intent);
                } else {
                    Snackbar.make(rootView,"Data tidak tersedia",Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return filterlist.size();
    }

    @Override
    public Filter getFilter(){
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence){
                String charString = charSequence.toString();
                if (charString.isEmpty()){
                    filterlist = comFas;
                } else {
                    List<DataObject> filteredlist = new ArrayList<>();
                    for (DataObject dataObject : comFas){
                        final String nama = dataObject.getNamComp().toLowerCase();
                        final String risk = dataObject.getRefRisk().toLowerCase();
                        if (nama.contains(charString.toLowerCase()) || risk.contains(charString.toLowerCase())){
                            filteredlist.add(dataObject);
                        }
                    }
                    filterlist = filteredlist;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filterlist;
                return filterResults;
            }
            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filterlist = (ArrayList<DataObject>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    private String tglHelper(String params){
        if (params != null && !params.isEmpty() && params != "null")
        {
            String[] sourceParams = params.split("-");
            String tahun = sourceParams[0];
            String bulan = sourceParams[1];
            String tangg = sourceParams[2].substring(0,2);

            String[] WaktuParams = params.split(" ");
            String waktu = WaktuParams[0].substring(11);
            String[] detWak = waktu.split(":");
            String jam = detWak[0];
            String menit = detWak[1];
            String detik = detWak[2].substring(0,2);

            return tangg + "/" + bulan + "/" + tahun + " " + jam + ":" + menit + ":" + detik + " WIB";
        } else {
            return "-";
        }
    }

}