package com.elwilis.emsitpro.trinitymobile;

/**
 * Created by rezar on 16/02/18.
 */

public class PesanObject {

    private String seqsNote, subjLaps, namaKant, namaComp, fasiComp, namaUser, dateNote, refsSess, refsRead;

    public String getSeqsNote() {
        return seqsNote;
    }

    public void setSeqsNote(String seqsNote) {
        this.seqsNote = seqsNote;
    }

    public String getSubjLaps() {
        return subjLaps;
    }

    public void  setSubjLaps(String subjLaps) {
        this.subjLaps = subjLaps;
    }

    public String getNamaKant() {
        return namaKant;
    }

    public void setNamaKant(String namaKant) {
        this.namaKant = namaKant;
    }

    public String getNamaComp() {
        return namaComp;
    }

    public void setNamaComp(String namaComp) {
        this.namaComp = namaComp;
    }

    public String getFasiComp() {
        return fasiComp;
    }

    public void setFasiComp(String fasiComp) {
        this.fasiComp = fasiComp;
    }

    public String getNamaUser() {
        return namaUser;
    }

    public void setNamaUser(String namaUser) {
        this.namaUser = namaUser;
    }

    public String getDateNote() {
        return dateNote;
    }

    public void setDateNote(String dateNote) {
        this.dateNote = dateNote;
    }

    public String getRefsSess() {
        return refsSess;
    }

    public void setRefsSess(String refsSess) {
        this.refsSess = refsSess;
    }

    public String getRefsRead() {
        return refsRead;
    }

    public void setRefsRead(String refsRead) {
        this.refsRead = refsRead;
    }

}
