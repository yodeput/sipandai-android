package com.elwilis.emsitpro.trinitymobile.skepmodel;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.elwilis.emsitpro.trinitymobile.R;
import java.util.ArrayList;

public class ExpListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private ArrayList<HeadModel> headModels;

    public ExpListAdapter(Context context, ArrayList<HeadModel> headModels) {
        this._context = context;
        this.headModels = headModels;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        ArrayList<Child> chList = headModels.get(groupPosition).getItems();
        return chList.get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        Child child = (Child) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert inflater != null;
            convertView = inflater.inflate(R.layout.skep_item, null);
        }

        TextView txtNama = convertView.findViewById(R.id.namComp);
        TextView txtDate = convertView.findViewById(R.id.tglSkep);

        txtNama.setText(child.getName());
        txtDate.setText(child.getCaption());

        return convertView;

    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<Child> chList = headModels.get(groupPosition).getItems();
        return chList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return headModels.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return headModels.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        HeadModel headModel = (HeadModel) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert inf != null;
            convertView = inf.inflate(R.layout.skep_head, null);
        }
        TextView tview = convertView.findViewById(R.id.headingTxt);
        tview.setText(headModel.getName());
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
