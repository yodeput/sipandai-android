package com.elwilis.emsitpro.trinitymobile.livereport;

/**
 * Created by rezar on 12/02/18.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import java.io.ByteArrayOutputStream;

public class Utils {
    public static final String urlUpload = "http://sipandaibcjabar.com/sprs/note";
    //public static final String urlUpload = "https://sipandai.p2bcjabar.com/sprs/note";
    // public static final String urlUpload = "http://192.168.1.101/sipandai/sprs/note";
    public static final int REQCODE = 100;
    public static final String imgFil = "imgsFile";
    public static final String imgNam = "imgsName";
    public static final String subRep = "subjRepo";
    public static final String conRep = "contRepo";
    public static final String tokEns = "tokeValu";
    public static final String usrNip = "refsUser";
    public static final String usrLoc = "refsKord";
    public static final String namCom = "namaComp";
    public static final String fasCom = "fasiComp";
    public static final String seqCom = "seqsComp";
    public static final String kanCom = "kantKpbc";

    public static byte[] getFileDataFromDrawable(Context context, int id) {
        Drawable drawable = ContextCompat.getDrawable(context, id);
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public static byte[] getFileDataFromDrawable(Context context, Drawable drawable) {
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

}
