package com.elwilis.emsitpro.trinitymobile;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by rezar on 29/01/18.
 */

public class FasHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView namComp, alaComp, refRisk, updDate, gpsLoca, refPoin, kodKant;

    ItemClickListener itemClickListener;

    public FasHolder(View itemView) {

        super(itemView);
        // card_row holder
        this.namComp = itemView.findViewById(R.id.nam_comp);
        this.alaComp = itemView.findViewById(R.id.ala_comp);
        this.refRisk = itemView.findViewById(R.id.ref_risk);
        this.updDate = itemView.findViewById(R.id.upd_date);
        this.gpsLoca = itemView.findViewById(R.id.gps_loca);
        this.refPoin = itemView.findViewById(R.id.ref_poin);
        this.kodKant = itemView.findViewById(R.id.ref_kant);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View rootView){
        this.itemClickListener.onItemClick(rootView,getLayoutPosition());
    }

    public void setItemClickListener(ItemClickListener ic){
        this.itemClickListener = ic;
    }
}
