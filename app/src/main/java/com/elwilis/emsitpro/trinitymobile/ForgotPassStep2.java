package com.elwilis.emsitpro.trinitymobile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ForgotPassStep2 extends AppCompatActivity {

    private Context mContext;
    private EditText ucode;
    private Button btnSecond;
    private ProgressDialog pDialog;
    private String ucodeHolder;
    private boolean checkEmpty;
    private String APIEndPoint;
    ConnectivityManager conMgr;
    SessionManage session;
    public static final String EXTRA_EMAIL = "com.elwilis.emsitpro.trinitymobile.email";
    public static final String EXTRA_MESSAGE = "com.elwilis.emsitpro.trinitymobile.message";
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass_step2);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = ForgotPassStep2.this;
        conMgr = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        session = new SessionManage(getApplicationContext());
        pDialog = new ProgressDialog(mContext);
        ucode = findViewById(R.id.ucode);
        btnSecond = findViewById(R.id.btnFor2);
        btnSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkField();
                TextInputLayout til = findViewById(R.id.errcod);
                if (checkEmpty) {
                    if (conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable() && conMgr.getActiveNetworkInfo().isConnected()){
                        checkUcode();
                    } else {
                        Toast.makeText(mContext, "Tidak ada koneksi internet.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    til.setError("Kode Unik wajib diisi.");
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    // post to API
    private void checkUcode(){
        Intent intent = getIntent();
        final String userMail = intent.getStringExtra(ForgotPassStep1.EXTRA_EMAIL);
        final String userCode = ucode.getText().toString().trim();
        pDialog.setMessage("Proses Permintaan...");
        showDialog();
        APIEndPoint = AppVar.RESP_URL;
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest strReq = new StringRequest(
                Request.Method.POST,
                APIEndPoint,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideDialog();
                        try {
                            JSONObject rsp = new JSONObject(response);
                            Boolean sukses = rsp.getBoolean("success");
                            String pesan = rsp.getString("message");
                            if (sukses == AppVar.RESPONSE_SUCCESS) {
                                Intent intent = new Intent(mContext, ForgotPassStep3.class);
                                intent.putExtra(EXTRA_EMAIL, userMail);
                                intent.putExtra(EXTRA_MESSAGE, userCode);
                                startActivity(intent);
                            } else {
                                Toast.makeText(ForgotPassStep2.this, pesan, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){
                        hideDialog();
                        Toast.makeText(ForgotPassStep2.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
        ){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("usrmail",userMail);
                params.put("usrhash",userCode);
                return params;
            }

            @Override
            public Map<String,String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        requestQueue.add(strReq);
    }

    private void checkField(){
        ucodeHolder = ucode.getText().toString().trim();
        checkEmpty = !TextUtils.isEmpty(ucodeHolder);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
