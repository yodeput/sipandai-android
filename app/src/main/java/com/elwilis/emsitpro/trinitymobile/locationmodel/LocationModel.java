package com.elwilis.emsitpro.trinitymobile.locationmodel;

/**
 * Created by rezar on 02/02/18.
 */

public class LocationModel {

    private Integer seqComp;
    private String namComp;
    private Double latComp;
    private Double lonComp;

    public LocationModel(Integer seqComp, String namComp, Double latComp, Double lonComp) {
        this.seqComp = seqComp;
        this.namComp = namComp;
        this.latComp = latComp;
        this.lonComp = lonComp;
    }

    public LocationModel() {

    }

    public Integer getSeqComp() {
        return seqComp;
    }

    public String getNamComp() {
        return namComp;
    }

    public Double getLatComp() {
        return latComp;
    }

    public Double getLonComp() {
        return lonComp;
    }

    public void setSeqComp(String seqComp) {
        this.seqComp = Integer.valueOf(seqComp);
    }

    public void setNamComp(String namComp) {
        this.namComp = namComp;
    }

    public void setLatComp(String latComp) {
        this.latComp = Double.valueOf(latComp);
    }

    public void setLonComp(String lonComp) {
        this.lonComp = Double.valueOf(lonComp);
    }
}
