package com.elwilis.emsitpro.trinitymobile.company;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.elwilis.emsitpro.trinitymobile.AppVar;
import com.elwilis.emsitpro.trinitymobile.CustomVolleyRequest;
import com.elwilis.emsitpro.trinitymobile.R;
import com.elwilis.emsitpro.trinitymobile.SessionManage;
import com.elwilis.emsitpro.trinitymobile.SliderUtils;
import com.elwilis.emsitpro.trinitymobile.ViewPagerAdapter;
import com.elwilis.emsitpro.trinitymobile.livereport.CompReportActivity;
import com.gdacciaro.iOSDialog.iOSDialog;
import com.gdacciaro.iOSDialog.iOSDialogBuilder;
import com.gdacciaro.iOSDialog.iOSDialogClickListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.rilixtech.materialfancybutton.MaterialFancyButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PtPs extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    // view
    @BindView(R.id.toolbar) Toolbar _toolbar;
    @BindView(R.id.img_comp) ViewPager _imgsComp;
    @BindView(R.id.SliderDots) LinearLayout _slidDots;
    @BindView(R.id.namaKpbc) TextView _namaKpbc;
    @BindView(R.id.namaComp) TextView _namaComp;
    @BindView(R.id.lastUpda) TextView _lastUpda;
    @BindView(R.id.nomoNpwp) TextView _nomoNpwp;
    @BindView(R.id.infoLeta) TextView _infoLeta;
    @BindView(R.id.det_nom_skp1) TextView _skepAwal;
    @BindView(R.id.det_tgl_skp1) TextView _skepTsta;
    @BindView(R.id.det_tgl_skp2) TextView _skepLast;
    @BindView(R.id.namaLoka) TextView _namaLoka;
    @BindView(R.id.ptpsPanj) TextView _ptpsPanj;
    @BindView(R.id.ptpsLeba) TextView _ptpsLeba;
    @BindView(R.id.ptpsLuas) TextView _ptpsLuas;
    @BindView(R.id.ptpsVolu) TextView _ptpsVolu;
    @BindView(R.id.ptpsSela) TextView _ptpsSela;
    @BindView(R.id.ptpsTimu) TextView _ptpsTimu;
    @BindView(R.id.ptpsBara) TextView _ptpsBara;
    @BindView(R.id.ptpsUtar) TextView _ptpsUtar;
    @BindView(R.id.ptpsJmas) TextView _ptpsJmas;
    @BindView(R.id.ptpsJkel) TextView _ptpsJkel;
    @BindView(R.id.ptpsPmas) TextView _ptpsPmas;
    @BindView(R.id.ptpsPkel) TextView _ptpsPkel;
    @BindView(R.id.ptpsDena) TextView _ptpsDena;
    @BindView(R.id.ptpsPics) TextView _ptpsPics;
    @BindView(R.id.ptpsTpic) TextView _ptpsTpic;
    @BindView(R.id.keteStat) TextView _keteStat;
    @BindView(R.id.naviPeta) MaterialFancyButton _naviPeta;
    @BindView(R.id.naviLoka) MaterialFancyButton _naviLoka;
    @BindView(R.id.sendRepo) MaterialFancyButton _sendRepo;

    // general
    Context context;
    ProgressDialog pDialog;
    SessionManage session;
    NumberFormat numberFormat;
    Locale locale;
    Intent intent;
    Bundle bundle;

    // holder
    int dotscount;
    ImageView[] dots;
    String tokEns, apiEnd, apiLok, formatDateTime, seqCom, extHol, usrNip, locLat, locLon, comLoc, namKan, namCom, kodKan;
    Double devLat, devLon;
    RequestQueue rq;
    List<SliderUtils> sliderImg;
    ViewPagerAdapter viewPagerAdapter;
    Map<String, String> headers;

    // location
    private Location mylocation;
    private GoogleApiClient googleApiClient;
    private static final String TAG = PtPs.class.getSimpleName();
    private final static int REQUEST_CHECK_SETTINGS_GPS=0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS=0x2;
    private boolean startedService = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pt_ps);
        ButterKnife.bind(this);
        setSupportActionBar(_toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        context = PtPs.this;
        session = new SessionManage(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        tokEns = user.get(SessionManage.KEY_NAME);
        usrNip = user.get(SessionManage.KEY_NIP);

        rq = CustomVolleyRequest.getInstance(this).getRequestQueue();
        pDialog = new ProgressDialog(this);
        sliderImg = new ArrayList<>();

        // get company sequence
        intent = getIntent();
        String param = intent.getStringExtra("refs");
        String [] params = param.split("-");
        seqCom = params[0];

        // viewpager
        _imgsComp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {

                for(int i = 0; i< dotscount; i++){
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.nonactive_dot));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));

            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        setUpGClient();

        pDialog.setMessage("Mohon tunggu...");
        showDialog();

        jsonData(seqCom);

        // tetapkan peta
        _naviPeta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mylocation != null) {
                    new iOSDialogBuilder(PtPs.this)
                            .setTitle("Perbarui koordinat GPS pengguna jasa?")
                            .setSubtitle("Koordinat GPS sebelumnya: \n" + cekNull(comLoc) + "\nKoordinat GPS saat ini: " + mylocation.getLatitude() + "," + mylocation.getLongitude())
                            .setBoldPositiveLabel(true)
                            .setCancelable(false)
                            .setPositiveListener(getString(R.string.ok), new iOSDialogClickListener() {
                                @Override
                                public void onClick(iOSDialog dialog) {
                                    pDialog.setMessage("Mohon tunggu...");
                                    showDialog();
                                    updaLoka();
                                    dialog.dismiss();
                                }
                            })
                            .setNegativeListener(getString(R.string.no), new iOSDialogClickListener() {
                                @Override
                                public void onClick(iOSDialog dialog) {
                                    dialog.dismiss();
                                }
                            }).build().show();
                } else {
                    new iOSDialogBuilder(PtPs.this)
                            .setTitle("Peringatan")
                            .setSubtitle("Lokasi Perangkat tidak tersedia, silahkan aktifkan GPS untuk mendapatkan lokasi saat ini.")
                            .setBoldPositiveLabel(true)
                            .setCancelable(false)
                            .setPositiveListener(getString(R.string.ok),new iOSDialogClickListener() {
                                @Override
                                public void onClick(iOSDialog dialog) {
                                    dialog.dismiss();
                                }
                            }).build().show();
                }
            }
        });

        // navigasi peta
        _naviLoka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (comLoc != null && !comLoc.equals("null") && !comLoc.equals("")) {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q="+comLoc+"");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                } else {
                    new iOSDialogBuilder(PtPs.this)
                            .setTitle("Informasi")
                            .setSubtitle("Koordinat GPS pengguna jasa belum tersedia. Gunakan tombol TETAPKAN PETA untuk mempebarui koordinat GPS.")
                            .setBoldPositiveLabel(true)
                            .setCancelable(false)
                            .setPositiveListener(getString(R.string.ok),new iOSDialogClickListener() {
                                @Override
                                public void onClick(iOSDialog dialog) {
                                    dialog.dismiss();
                                }
                            }).build().show();
                }
            }
        });

        // buat laporan
        _sendRepo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dataIntent = new Intent(PtPs.this, CompReportActivity.class);
                dataIntent.putExtra("seqsComp", String.valueOf(seqCom));
                dataIntent.putExtra("namaComp", namCom);
                dataIntent.putExtra("fasiComp","PTPS");
                dataIntent.putExtra("kantComp", namKan);
                dataIntent.putExtra("userNips", usrNip);
                dataIntent.putExtra("rkanComp", kodKan);
                startActivity(dataIntent);
            }
        });

    }

    private synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        mylocation = location;
    }

    @Override
    public void onConnected(Bundle bundle) {
        checkPermissions();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Do whatever you need
        //You can display a message here
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //You can display a message here
    }

    private void getMyLocation(){
        if(googleApiClient!=null) {
            if (googleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(PtPs.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == android.content.pm.PackageManager.PERMISSION_GRANTED) {
                    mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(10000);
                    locationRequest.setFastestInterval(5000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
                    PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                        @Override
                        public void onResult(LocationSettingsResult result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    int permissionLocation = ContextCompat.checkSelfPermission(PtPs.this, Manifest.permission.ACCESS_FINE_LOCATION);
                                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                        mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(PtPs.this, REQUEST_CHECK_SETTINGS_GPS);
                                    } catch (android.content.IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    break;
                            }
                        }
                    });
                }

            }
        }
    }

    private void checkPermissions(){

        int permissionLocation = ContextCompat.checkSelfPermission(PtPs.this, Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            }
        } else {
            getMyLocation();
        }

    }

    private void jsonData(final String seqCom) {
        apiEnd = AppVar.GETS_TP;
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                apiEnd,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideDialog();
                        try {
                            Boolean sukses = response.getBoolean("success");
                            String pesan = response.getString("message");
                            if (sukses) {
                                JSONObject data = response.getJSONObject("data");

                                kodKan = data.getString("nam_kant");
                                comLoc = data.getString("gps_kord");
                                namKan = data.getString("ref_kant");
                                namCom = String.format("%s, %s", data.getString("nam_ptps"), data.getString("nam_baus"));

                                _namaKpbc.setText(data.getString("ref_kant"));
                                _namaComp.setText(String.format("%s, %s", data.getString("nam_ptps"), data.getString("nam_baus")));
                                _lastUpda.setText(String.format("Pembaruan akhir: %s", tglHelper(data.getString("tgl_upda"))));
                                _nomoNpwp.setText(data.getString("nom_npwp"));
                                _namaLoka.setText(data.getString("ket_loka"));
                                _skepAwal.setText(data.getString("nom_skep"));
                                _skepTsta.setText(tglOnly(data.getString("tgl_skep")));
                                _skepLast.setText(tglOnly(data.getString("akh_skep")));
                                _infoLeta.setText(data.getString("let_loka"));
                                _ptpsPanj.setText(data.getString("num_panj"));
                                _ptpsLeba.setText(data.getString("num_leba"));
                                _ptpsLuas.setText(data.getString("num_luas"));
                                _ptpsVolu.setText(data.getString("num_volu"));
                                _ptpsSela.setText(data.getString("bat_sela"));
                                _ptpsTimu.setText(data.getString("bat_timu"));
                                _ptpsBara.setText(data.getString("bat_bara"));
                                _ptpsUtar.setText(data.getString("bat_utar"));
                                _ptpsJmas.setText(data.getString("jum_pmas"));
                                _ptpsJkel.setText(data.getString("jum_pkel"));
                                _ptpsDena.setText(enumAva(data.getString("ref_dena")));
                                _ptpsPmas.setText(enumAva(data.getString("jum_muta")));
                                _ptpsPkel.setText(enumAva(data.getString("jum_kuta")));
                                _ptpsPics.setText(data.getString("nam_pics"));
                                _ptpsTpic.setText(data.getString("nom_telp"));
                                _keteStat.setText(cekNull(data.getString("ket_ptps")));

                                JSONArray foto = data.getJSONArray("img_data");
                                for (int i = 0; i < foto.length(); i++){
                                    try {
                                        JSONObject jsonObject = foto.getJSONObject(i);
                                        SliderUtils sliderUtils = new SliderUtils();
                                        sliderUtils.setSliderImageUrl(jsonObject.getString("url_repo"));
                                        sliderImg.add(sliderUtils);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                viewPagerAdapter = new ViewPagerAdapter(sliderImg, getApplicationContext());
                                _imgsComp.setAdapter(viewPagerAdapter);

                                dotscount = viewPagerAdapter.getCount();
                                dots = new ImageView[dotscount];

                                for(int i = 0; i < dotscount; i++){
                                    dots[i] = new ImageView(getApplicationContext());
                                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.nonactive_dot));
                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                    params.setMargins(8, 0, 8, 0);
                                    _slidDots.addView(dots[i], params);
                                }
                                dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));
                            } else {
                                Toast.makeText(context, pesan, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError){
                    Toast.makeText(context, "Connection time out", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(context, "Server problem", Toast.LENGTH_SHORT).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(context, "Internet problem", Toast.LENGTH_SHORT).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(context, "Parsing problem", Toast.LENGTH_SHORT).show();
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() {
                headers = new HashMap<String, String>();
                headers.put("x-access-token", tokEns);
                headers.put("sequence", seqCom);
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    // location
    private void updaLoka() {
        apiLok = AppVar.UGPS_TP;
        final String newLoc = mylocation.getLatitude() + "," + mylocation.getLongitude();
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                apiLok,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        try {
                            hideDialog();
                            JSONObject rsp = new JSONObject(response);
                            Boolean sukses = rsp.getBoolean("success");
                            String pesan = rsp.getString("message");
                            Toast.makeText(getApplicationContext(), pesan, Toast.LENGTH_SHORT).show();
                            finish();
                            startActivity(getIntent());
                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError || error instanceof NoConnectionError){
                            Toast.makeText(context, "Connection time out", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(context, "Server problem", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(context, "Internet problem", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(context, "Parsing problem", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        ){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("kantor", String.valueOf(kodKan));
                params.put("sequence", seqCom);
                params.put("newgpskor", newLoc);
                params.put("token", tokEns);
                return params;
            }
            @Override
            public Map<String,String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private String tglHelper(String params){
        if (params != null && !params.isEmpty() && params != "null")
        {
            String[] sourceParams = params.split("-");
            String tahun = sourceParams[0];
            String bulan = sourceParams[1];
            String tangg = sourceParams[2].substring(0,2);

            String[] WaktuParams = params.split(" ");
            String waktu = WaktuParams[0].substring(11);
            String[] detWak = waktu.split(":");
            String jam = detWak[0];
            String menit = detWak[1];
            String detik = detWak[2].substring(0,2);

            return tangg + "/" + bulan + "/" + tahun + " " + jam + ":" + menit + ":" + detik + " WIB";
        } else {
            return "-";
        }
    }

    private String tglOnly(String params){
        if (params != null && !params.isEmpty() && params != "null")
        {
            String[] sourceParams = params.split("-");
            String tahun = sourceParams[0];
            String bulan = sourceParams[1];
            String tangg = sourceParams[2].substring(0,2);
            return tangg + "/" + bulan + "/" + tahun;
        } else {
            return "-";
        }
    }

    private String cekNull(String params){
        if (params != null && !params.equals("") && !params.equals("null")){
            return params;
        } else {
            return "-";
        }
    }

    private String enumAva(String params) {
        if (params.equals("1")) {
            return "Ada";
        } else {
            return "Tidak Ada";
        }
    }
}
