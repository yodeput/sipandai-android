package com.elwilis.emsitpro.trinitymobile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ForgotPassStep3 extends AppCompatActivity {

    private Context mContext;
    private EditText npass;
    private EditText cpass;
    private Button btnThird;
    private ProgressDialog pDialog;
    private String npassHolder;
    private String cpassHolder;
    private boolean checkEmpty;
    private boolean checkEqu;
    private String APIEndPoint;
    ConnectivityManager conMgr;
    SessionManage session;
    public static final String EXTRA_EMAIL = "com.elwilis.emsitpro.trinitymobile.email";
    public static final String EXTRA_MESSAGE = "com.elwilis.emsitpro.trinitymobile.message";
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass_step3);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mContext = ForgotPassStep3.this;
        conMgr = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        session = new SessionManage(getApplicationContext());
        pDialog = new ProgressDialog(mContext);
        npass = findViewById(R.id.npass);
        cpass = findViewById(R.id.cpass);
        btnThird = findViewById(R.id.btnFor3);

        btnThird.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkField();
                TextInputLayout npw = findViewById(R.id.errnpw);
                TextInputLayout cpw = findViewById(R.id.errcpw);
                if (checkEmpty) {
                    if (npass.getText().length() <= 6){
                        npw.setError("Kata sandi harus lebih dari 6 karakter.");
                    } else {
                        checkEqual();
                        if (checkEqu) {
                            if (conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable() && conMgr.getActiveNetworkInfo().isConnected()){
                                updatPassw();
                            } else {
                                Toast.makeText(mContext, "Tidak ada koneksi internet.", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            npw.setError("Kata Sandi tidak sama.");
                            cpw.setError("Konfirmasi Kata Sandi tidak sama.");
                        }
                    }
                } else {
                    npw.setError("Kata Sandi wajib diisi.");
                    cpw.setError("Konfirmasi Kata Sandi wajib diisi.");
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void updatPassw() {
        Intent intent = getIntent();
        final String userMail = intent.getStringExtra(ForgotPassStep2.EXTRA_EMAIL);
        final String userCode = intent.getStringExtra(ForgotPassStep2.EXTRA_MESSAGE);
        final String userPass = npass.getText().toString().trim();
        pDialog.setMessage("Proses Permintaan...");
        showDialog();
        APIEndPoint = AppVar.NEWP_URL;
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest strReq = new StringRequest(
                Request.Method.POST,
                APIEndPoint,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideDialog();
                        try {
                            JSONObject rsp = new JSONObject(response);
                            Boolean sukses = rsp.getBoolean("success");
                            String pesan = rsp.getString("message");
                            if (sukses) {
                                Toast.makeText(ForgotPassStep3.this, pesan, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(mContext, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            } else {
                                Toast.makeText(ForgotPassStep3.this, pesan, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){
                        hideDialog();
                        Toast.makeText(ForgotPassStep3.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
        ){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("usrmail",userMail);
                params.put("usrhash",userCode);
                params.put("usrnpas",userPass);
                return params;
            }

            @Override
            public Map<String,String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        requestQueue.add(strReq);
    }

    private void checkField(){
        npassHolder = npass.getText().toString().trim();
        cpassHolder = cpass.getText().toString().trim();
        checkEmpty = !(TextUtils.isEmpty(npassHolder) || TextUtils.isEmpty(cpassHolder));
    }

    private void checkEqual(){
        npassHolder = npass.getText().toString().trim();
        cpassHolder = cpass.getText().toString().trim();
        checkEqu = TextUtils.equals(npassHolder, cpassHolder);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
